/**
 * Copyright (c) 2022 DB Netz AG and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package org.eclipse.set.model.model1902.Balisentechnik_ETCS;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>ENUMDP Typ ESG</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.set.model.model1902.Balisentechnik_ETCS.Balisentechnik_ETCSPackage#getENUMDPTypESG()
 * @model extendedMetaData="name='ENUMDP_Typ_ESG'"
 * @generated
 */
public enum ENUMDPTypESG implements Enumerator {
	/**
	 * The '<em><b>ENUMDP Typ ESG AB</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AB_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_AB(0, "ENUMDP_Typ_ESG_AB", "AB"),

	/**
	 * The '<em><b>ENUMDP Typ ESG AE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AE_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_AE(1, "ENUMDP_Typ_ESG_AE", "AE"),

	/**
	 * The '<em><b>ENUMDP Typ ESG AF</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AF_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_AF(2, "ENUMDP_Typ_ESG_AF", "AF"),

	/**
	 * The '<em><b>ENUMDP Typ ESG AS 136</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AS_136_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_AS_136(3, "ENUMDP_Typ_ESG_AS_136", "AS-136"),

	/**
	 * The '<em><b>ENUMDP Typ ESG AS 187</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AS_187_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_AS_187(4, "ENUMDP_Typ_ESG_AS_187", "AS-187"),

	/**
	 * The '<em><b>ENUMDP Typ ESG AS 24</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AS_24_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_AS_24(5, "ENUMDP_Typ_ESG_AS_24", "AS-24"),

	/**
	 * The '<em><b>ENUMDP Typ ESG AS 48</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AS_48_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_AS_48(6, "ENUMDP_Typ_ESG_AS_48", "AS-48"),

	/**
	 * The '<em><b>ENUMDP Typ ESG AS 72</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AS_72_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_AS_72(7, "ENUMDP_Typ_ESG_AS_72", "AS-72"),

	/**
	 * The '<em><b>ENUMDP Typ ESG AS 99</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AS_99_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_AS_99(8, "ENUMDP_Typ_ESG_AS_99", "AS-99"),

	/**
	 * The '<em><b>ENUMDP Typ ESG AW</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AW_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_AW(9, "ENUMDP_Typ_ESG_AW", "AW"),

	/**
	 * The '<em><b>ENUMDP Typ ESG BA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_BA_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_BA(10, "ENUMDP_Typ_ESG_BA", "BA"),

	/**
	 * The '<em><b>ENUMDP Typ ESG BM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_BM_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_BM(11, "ENUMDP_Typ_ESG_BM", "BM"),

	/**
	 * The '<em><b>ENUMDP Typ ESG BP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_BP_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_BP(12, "ENUMDP_Typ_ESG_BP", "BP"),

	/**
	 * The '<em><b>ENUMDP Typ ESG BR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_BR_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_BR(13, "ENUMDP_Typ_ESG_BR", "BR"),

	/**
	 * The '<em><b>ENUMDP Typ ESG BS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_BS_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_BS(14, "ENUMDP_Typ_ESG_BS", "BS"),

	/**
	 * The '<em><b>ENUMDP Typ ESG FH1</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_FH1_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_FH1(15, "ENUMDP_Typ_ESG_FH1", "FH1"),

	/**
	 * The '<em><b>ENUMDP Typ ESG FH2</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_FH2_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_FH2(16, "ENUMDP_Typ_ESG_FH2", "FH2"),

	/**
	 * The '<em><b>ENUMDP Typ ESG FM1</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_FM1_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_FM1(17, "ENUMDP_Typ_ESG_FM1", "FM1"),

	/**
	 * The '<em><b>ENUMDP Typ ESG FM2</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_FM2_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_FM2(18, "ENUMDP_Typ_ESG_FM2", "FM2"),

	/**
	 * The '<em><b>ENUMDP Typ ESG GH1</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GH1_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_GH1(19, "ENUMDP_Typ_ESG_GH1", "GH1"),

	/**
	 * The '<em><b>ENUMDP Typ ESG GH2</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GH2_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_GH2(20, "ENUMDP_Typ_ESG_GH2", "GH2"),

	/**
	 * The '<em><b>ENUMDP Typ ESG GH3</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GH3_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_GH3(21, "ENUMDP_Typ_ESG_GH3", "GH3"),

	/**
	 * The '<em><b>ENUMDP Typ ESG GH4</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GH4_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_GH4(22, "ENUMDP_Typ_ESG_GH4", "GH4"),

	/**
	 * The '<em><b>ENUMDP Typ ESG GH5</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GH5_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_GH5(23, "ENUMDP_Typ_ESG_GH5", "GH5"),

	/**
	 * The '<em><b>ENUMDP Typ ESG GM1</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GM1_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_GM1(24, "ENUMDP_Typ_ESG_GM1", "GM1"),

	/**
	 * The '<em><b>ENUMDP Typ ESG GM2</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GM2_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_GM2(25, "ENUMDP_Typ_ESG_GM2", "GM2"),

	/**
	 * The '<em><b>ENUMDP Typ ESG GM3</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GM3_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_GM3(26, "ENUMDP_Typ_ESG_GM3", "GM3"),

	/**
	 * The '<em><b>ENUMDP Typ ESG GM4</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GM4_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_GM4(27, "ENUMDP_Typ_ESG_GM4", "GM4"),

	/**
	 * The '<em><b>ENUMDP Typ ESG GM5</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GM5_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_GM5(28, "ENUMDP_Typ_ESG_GM5", "GM5"),

	/**
	 * The '<em><b>ENUMDP Typ ESG HS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_HS_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_HS(29, "ENUMDP_Typ_ESG_HS", "HS"),

	/**
	 * The '<em><b>ENUMDP Typ ESG LB</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_LB_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_LB(30, "ENUMDP_Typ_ESG_LB", "LB"),

	/**
	 * The '<em><b>ENUMDP Typ ESG LF</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_LF_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_LF(31, "ENUMDP_Typ_ESG_LF", "LF"),

	/**
	 * The '<em><b>ENUMDP Typ ESG LR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_LR_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_LR(32, "ENUMDP_Typ_ESG_LR", "LR"),

	/**
	 * The '<em><b>ENUMDP Typ ESG MS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_MS_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_MS(33, "ENUMDP_Typ_ESG_MS", "MS"),

	/**
	 * The '<em><b>ENUMDP Typ ESG NV</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_NV_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_NV(34, "ENUMDP_Typ_ESG_NV", "NV"),

	/**
	 * The '<em><b>ENUMDP Typ ESG RP1</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_RP1_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_RP1(35, "ENUMDP_Typ_ESG_RP1", "RP1"),

	/**
	 * The '<em><b>ENUMDP Typ ESG RP2</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_RP2_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_RP2(36, "ENUMDP_Typ_ESG_RP2", "RP2"),

	/**
	 * The '<em><b>ENUMDP Typ ESG RS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_RS_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_RS(37, "ENUMDP_Typ_ESG_RS", "RS"),

	/**
	 * The '<em><b>ENUMDP Typ ESG SA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SA_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_SA(38, "ENUMDP_Typ_ESG_SA", "SA"),

	/**
	 * The '<em><b>ENUMDP Typ ESG SB</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SB_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_SB(39, "ENUMDP_Typ_ESG_SB", "SB"),

	/**
	 * The '<em><b>ENUMDP Typ ESG SK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SK_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_SK(40, "ENUMDP_Typ_ESG_SK", "SK"),

	/**
	 * The '<em><b>ENUMDP Typ ESG SN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SN_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_SN(41, "ENUMDP_Typ_ESG_SN", "SN"),

	/**
	 * The '<em><b>ENUMDP Typ ESG sonstige</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SONSTIGE_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_SONSTIGE(42, "ENUMDP_Typ_ESG_sonstige", "sonstige"),

	/**
	 * The '<em><b>ENUMDP Typ ESG SP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SP_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_SP(43, "ENUMDP_Typ_ESG_SP", "SP"),

	/**
	 * The '<em><b>ENUMDP Typ ESG SS 136</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SS_136_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_SS_136(44, "ENUMDP_Typ_ESG_SS_136", "SS-136"),

	/**
	 * The '<em><b>ENUMDP Typ ESG SS 187</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SS_187_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_SS_187(45, "ENUMDP_Typ_ESG_SS_187", "SS-187"),

	/**
	 * The '<em><b>ENUMDP Typ ESG SS 24</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SS_24_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_SS_24(46, "ENUMDP_Typ_ESG_SS_24", "SS-24"),

	/**
	 * The '<em><b>ENUMDP Typ ESG SS 48</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SS_48_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_SS_48(47, "ENUMDP_Typ_ESG_SS_48", "SS-48"),

	/**
	 * The '<em><b>ENUMDP Typ ESG SS 72</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SS_72_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_SS_72(48, "ENUMDP_Typ_ESG_SS_72", "SS-72"),

	/**
	 * The '<em><b>ENUMDP Typ ESG SS 99</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SS_99_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_SS_99(49, "ENUMDP_Typ_ESG_SS_99", "SS-99"),

	/**
	 * The '<em><b>ENUMDP Typ ESG ST</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_ST_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_ST(50, "ENUMDP_Typ_ESG_ST", "ST"),

	/**
	 * The '<em><b>ENUMDP Typ ESG TA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TA_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_TA(51, "ENUMDP_Typ_ESG_TA", "TA"),

	/**
	 * The '<em><b>ENUMDP Typ ESG TE E</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TE_E_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_TE_E(52, "ENUMDP_Typ_ESG_TE_E", "TE-E"),

	/**
	 * The '<em><b>ENUMDP Typ ESG TE L</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TE_L_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_TE_L(53, "ENUMDP_Typ_ESG_TE_L", "TE-L"),

	/**
	 * The '<em><b>ENUMDP Typ ESG TE2</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TE2_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_TE2(54, "ENUMDP_Typ_ESG_TE2", "TE2"),

	/**
	 * The '<em><b>ENUMDP Typ ESG TG</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TG_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_TG(55, "ENUMDP_Typ_ESG_TG", "TG"),

	/**
	 * The '<em><b>ENUMDP Typ ESG TM E</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TM_E_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_TM_E(56, "ENUMDP_Typ_ESG_TM_E", "TM-E"),

	/**
	 * The '<em><b>ENUMDP Typ ESG TM L</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TM_L_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_TM_L(57, "ENUMDP_Typ_ESG_TM_L", "TM-L"),

	/**
	 * The '<em><b>ENUMDP Typ ESG TP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TP_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_TP(58, "ENUMDP_Typ_ESG_TP", "TP"),

	/**
	 * The '<em><b>ENUMDP Typ ESG TR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TR_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_TR(59, "ENUMDP_Typ_ESG_TR", "TR"),

	/**
	 * The '<em><b>ENUMDP Typ ESG TS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TS_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_TS(60, "ENUMDP_Typ_ESG_TS", "TS"),

	/**
	 * The '<em><b>ENUMDP Typ ESG TV E</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TV_E_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_TV_E(61, "ENUMDP_Typ_ESG_TV_E", "TV-E"),

	/**
	 * The '<em><b>ENUMDP Typ ESG TV L</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TV_L_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_TV_L(62, "ENUMDP_Typ_ESG_TV_L", "TV-L"),

	/**
	 * The '<em><b>ENUMDP Typ ESG VS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_VS_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_VS(63, "ENUMDP_Typ_ESG_VS", "VS"),

	/**
	 * The '<em><b>ENUMDP Typ ESG VW</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_VW_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMDP_TYP_ESG_VW(64, "ENUMDP_Typ_ESG_VW", "VW");

	/**
	 * The '<em><b>ENUMDP Typ ESG AB</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AB
	 * @model name="ENUMDP_Typ_ESG_AB" literal="AB"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_AB_VALUE = 0;

	/**
	 * The '<em><b>ENUMDP Typ ESG AE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AE
	 * @model name="ENUMDP_Typ_ESG_AE" literal="AE"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_AE_VALUE = 1;

	/**
	 * The '<em><b>ENUMDP Typ ESG AF</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AF
	 * @model name="ENUMDP_Typ_ESG_AF" literal="AF"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_AF_VALUE = 2;

	/**
	 * The '<em><b>ENUMDP Typ ESG AS 136</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AS_136
	 * @model name="ENUMDP_Typ_ESG_AS_136" literal="AS-136"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_AS_136_VALUE = 3;

	/**
	 * The '<em><b>ENUMDP Typ ESG AS 187</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AS_187
	 * @model name="ENUMDP_Typ_ESG_AS_187" literal="AS-187"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_AS_187_VALUE = 4;

	/**
	 * The '<em><b>ENUMDP Typ ESG AS 24</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AS_24
	 * @model name="ENUMDP_Typ_ESG_AS_24" literal="AS-24"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_AS_24_VALUE = 5;

	/**
	 * The '<em><b>ENUMDP Typ ESG AS 48</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AS_48
	 * @model name="ENUMDP_Typ_ESG_AS_48" literal="AS-48"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_AS_48_VALUE = 6;

	/**
	 * The '<em><b>ENUMDP Typ ESG AS 72</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AS_72
	 * @model name="ENUMDP_Typ_ESG_AS_72" literal="AS-72"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_AS_72_VALUE = 7;

	/**
	 * The '<em><b>ENUMDP Typ ESG AS 99</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AS_99
	 * @model name="ENUMDP_Typ_ESG_AS_99" literal="AS-99"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_AS_99_VALUE = 8;

	/**
	 * The '<em><b>ENUMDP Typ ESG AW</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_AW
	 * @model name="ENUMDP_Typ_ESG_AW" literal="AW"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_AW_VALUE = 9;

	/**
	 * The '<em><b>ENUMDP Typ ESG BA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_BA
	 * @model name="ENUMDP_Typ_ESG_BA" literal="BA"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_BA_VALUE = 10;

	/**
	 * The '<em><b>ENUMDP Typ ESG BM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_BM
	 * @model name="ENUMDP_Typ_ESG_BM" literal="BM"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_BM_VALUE = 11;

	/**
	 * The '<em><b>ENUMDP Typ ESG BP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_BP
	 * @model name="ENUMDP_Typ_ESG_BP" literal="BP"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_BP_VALUE = 12;

	/**
	 * The '<em><b>ENUMDP Typ ESG BR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_BR
	 * @model name="ENUMDP_Typ_ESG_BR" literal="BR"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_BR_VALUE = 13;

	/**
	 * The '<em><b>ENUMDP Typ ESG BS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_BS
	 * @model name="ENUMDP_Typ_ESG_BS" literal="BS"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_BS_VALUE = 14;

	/**
	 * The '<em><b>ENUMDP Typ ESG FH1</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_FH1
	 * @model name="ENUMDP_Typ_ESG_FH1" literal="FH1"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_FH1_VALUE = 15;

	/**
	 * The '<em><b>ENUMDP Typ ESG FH2</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_FH2
	 * @model name="ENUMDP_Typ_ESG_FH2" literal="FH2"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_FH2_VALUE = 16;

	/**
	 * The '<em><b>ENUMDP Typ ESG FM1</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_FM1
	 * @model name="ENUMDP_Typ_ESG_FM1" literal="FM1"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_FM1_VALUE = 17;

	/**
	 * The '<em><b>ENUMDP Typ ESG FM2</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_FM2
	 * @model name="ENUMDP_Typ_ESG_FM2" literal="FM2"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_FM2_VALUE = 18;

	/**
	 * The '<em><b>ENUMDP Typ ESG GH1</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GH1
	 * @model name="ENUMDP_Typ_ESG_GH1" literal="GH1"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_GH1_VALUE = 19;

	/**
	 * The '<em><b>ENUMDP Typ ESG GH2</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GH2
	 * @model name="ENUMDP_Typ_ESG_GH2" literal="GH2"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_GH2_VALUE = 20;

	/**
	 * The '<em><b>ENUMDP Typ ESG GH3</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GH3
	 * @model name="ENUMDP_Typ_ESG_GH3" literal="GH3"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_GH3_VALUE = 21;

	/**
	 * The '<em><b>ENUMDP Typ ESG GH4</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GH4
	 * @model name="ENUMDP_Typ_ESG_GH4" literal="GH4"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_GH4_VALUE = 22;

	/**
	 * The '<em><b>ENUMDP Typ ESG GH5</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GH5
	 * @model name="ENUMDP_Typ_ESG_GH5" literal="GH5"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_GH5_VALUE = 23;

	/**
	 * The '<em><b>ENUMDP Typ ESG GM1</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GM1
	 * @model name="ENUMDP_Typ_ESG_GM1" literal="GM1"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_GM1_VALUE = 24;

	/**
	 * The '<em><b>ENUMDP Typ ESG GM2</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GM2
	 * @model name="ENUMDP_Typ_ESG_GM2" literal="GM2"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_GM2_VALUE = 25;

	/**
	 * The '<em><b>ENUMDP Typ ESG GM3</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GM3
	 * @model name="ENUMDP_Typ_ESG_GM3" literal="GM3"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_GM3_VALUE = 26;

	/**
	 * The '<em><b>ENUMDP Typ ESG GM4</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GM4
	 * @model name="ENUMDP_Typ_ESG_GM4" literal="GM4"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_GM4_VALUE = 27;

	/**
	 * The '<em><b>ENUMDP Typ ESG GM5</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_GM5
	 * @model name="ENUMDP_Typ_ESG_GM5" literal="GM5"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_GM5_VALUE = 28;

	/**
	 * The '<em><b>ENUMDP Typ ESG HS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_HS
	 * @model name="ENUMDP_Typ_ESG_HS" literal="HS"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_HS_VALUE = 29;

	/**
	 * The '<em><b>ENUMDP Typ ESG LB</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_LB
	 * @model name="ENUMDP_Typ_ESG_LB" literal="LB"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_LB_VALUE = 30;

	/**
	 * The '<em><b>ENUMDP Typ ESG LF</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_LF
	 * @model name="ENUMDP_Typ_ESG_LF" literal="LF"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_LF_VALUE = 31;

	/**
	 * The '<em><b>ENUMDP Typ ESG LR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_LR
	 * @model name="ENUMDP_Typ_ESG_LR" literal="LR"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_LR_VALUE = 32;

	/**
	 * The '<em><b>ENUMDP Typ ESG MS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_MS
	 * @model name="ENUMDP_Typ_ESG_MS" literal="MS"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_MS_VALUE = 33;

	/**
	 * The '<em><b>ENUMDP Typ ESG NV</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_NV
	 * @model name="ENUMDP_Typ_ESG_NV" literal="NV"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_NV_VALUE = 34;

	/**
	 * The '<em><b>ENUMDP Typ ESG RP1</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_RP1
	 * @model name="ENUMDP_Typ_ESG_RP1" literal="RP1"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_RP1_VALUE = 35;

	/**
	 * The '<em><b>ENUMDP Typ ESG RP2</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_RP2
	 * @model name="ENUMDP_Typ_ESG_RP2" literal="RP2"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_RP2_VALUE = 36;

	/**
	 * The '<em><b>ENUMDP Typ ESG RS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_RS
	 * @model name="ENUMDP_Typ_ESG_RS" literal="RS"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_RS_VALUE = 37;

	/**
	 * The '<em><b>ENUMDP Typ ESG SA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SA
	 * @model name="ENUMDP_Typ_ESG_SA" literal="SA"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_SA_VALUE = 38;

	/**
	 * The '<em><b>ENUMDP Typ ESG SB</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SB
	 * @model name="ENUMDP_Typ_ESG_SB" literal="SB"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_SB_VALUE = 39;

	/**
	 * The '<em><b>ENUMDP Typ ESG SK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SK
	 * @model name="ENUMDP_Typ_ESG_SK" literal="SK"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_SK_VALUE = 40;

	/**
	 * The '<em><b>ENUMDP Typ ESG SN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SN
	 * @model name="ENUMDP_Typ_ESG_SN" literal="SN"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_SN_VALUE = 41;

	/**
	 * The '<em><b>ENUMDP Typ ESG sonstige</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SONSTIGE
	 * @model name="ENUMDP_Typ_ESG_sonstige" literal="sonstige"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_SONSTIGE_VALUE = 42;

	/**
	 * The '<em><b>ENUMDP Typ ESG SP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SP
	 * @model name="ENUMDP_Typ_ESG_SP" literal="SP"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_SP_VALUE = 43;

	/**
	 * The '<em><b>ENUMDP Typ ESG SS 136</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SS_136
	 * @model name="ENUMDP_Typ_ESG_SS_136" literal="SS-136"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_SS_136_VALUE = 44;

	/**
	 * The '<em><b>ENUMDP Typ ESG SS 187</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SS_187
	 * @model name="ENUMDP_Typ_ESG_SS_187" literal="SS-187"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_SS_187_VALUE = 45;

	/**
	 * The '<em><b>ENUMDP Typ ESG SS 24</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SS_24
	 * @model name="ENUMDP_Typ_ESG_SS_24" literal="SS-24"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_SS_24_VALUE = 46;

	/**
	 * The '<em><b>ENUMDP Typ ESG SS 48</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SS_48
	 * @model name="ENUMDP_Typ_ESG_SS_48" literal="SS-48"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_SS_48_VALUE = 47;

	/**
	 * The '<em><b>ENUMDP Typ ESG SS 72</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SS_72
	 * @model name="ENUMDP_Typ_ESG_SS_72" literal="SS-72"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_SS_72_VALUE = 48;

	/**
	 * The '<em><b>ENUMDP Typ ESG SS 99</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_SS_99
	 * @model name="ENUMDP_Typ_ESG_SS_99" literal="SS-99"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_SS_99_VALUE = 49;

	/**
	 * The '<em><b>ENUMDP Typ ESG ST</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_ST
	 * @model name="ENUMDP_Typ_ESG_ST" literal="ST"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_ST_VALUE = 50;

	/**
	 * The '<em><b>ENUMDP Typ ESG TA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TA
	 * @model name="ENUMDP_Typ_ESG_TA" literal="TA"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_TA_VALUE = 51;

	/**
	 * The '<em><b>ENUMDP Typ ESG TE E</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TE_E
	 * @model name="ENUMDP_Typ_ESG_TE_E" literal="TE-E"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_TE_E_VALUE = 52;

	/**
	 * The '<em><b>ENUMDP Typ ESG TE L</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TE_L
	 * @model name="ENUMDP_Typ_ESG_TE_L" literal="TE-L"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_TE_L_VALUE = 53;

	/**
	 * The '<em><b>ENUMDP Typ ESG TE2</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TE2
	 * @model name="ENUMDP_Typ_ESG_TE2" literal="TE2"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_TE2_VALUE = 54;

	/**
	 * The '<em><b>ENUMDP Typ ESG TG</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TG
	 * @model name="ENUMDP_Typ_ESG_TG" literal="TG"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_TG_VALUE = 55;

	/**
	 * The '<em><b>ENUMDP Typ ESG TM E</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TM_E
	 * @model name="ENUMDP_Typ_ESG_TM_E" literal="TM-E"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_TM_E_VALUE = 56;

	/**
	 * The '<em><b>ENUMDP Typ ESG TM L</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TM_L
	 * @model name="ENUMDP_Typ_ESG_TM_L" literal="TM-L"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_TM_L_VALUE = 57;

	/**
	 * The '<em><b>ENUMDP Typ ESG TP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TP
	 * @model name="ENUMDP_Typ_ESG_TP" literal="TP"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_TP_VALUE = 58;

	/**
	 * The '<em><b>ENUMDP Typ ESG TR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TR
	 * @model name="ENUMDP_Typ_ESG_TR" literal="TR"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_TR_VALUE = 59;

	/**
	 * The '<em><b>ENUMDP Typ ESG TS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TS
	 * @model name="ENUMDP_Typ_ESG_TS" literal="TS"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_TS_VALUE = 60;

	/**
	 * The '<em><b>ENUMDP Typ ESG TV E</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TV_E
	 * @model name="ENUMDP_Typ_ESG_TV_E" literal="TV-E"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_TV_E_VALUE = 61;

	/**
	 * The '<em><b>ENUMDP Typ ESG TV L</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_TV_L
	 * @model name="ENUMDP_Typ_ESG_TV_L" literal="TV-L"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_TV_L_VALUE = 62;

	/**
	 * The '<em><b>ENUMDP Typ ESG VS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_VS
	 * @model name="ENUMDP_Typ_ESG_VS" literal="VS"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_VS_VALUE = 63;

	/**
	 * The '<em><b>ENUMDP Typ ESG VW</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMDP_TYP_ESG_VW
	 * @model name="ENUMDP_Typ_ESG_VW" literal="VW"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMDP_TYP_ESG_VW_VALUE = 64;

	/**
	 * An array of all the '<em><b>ENUMDP Typ ESG</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ENUMDPTypESG[] VALUES_ARRAY =
		new ENUMDPTypESG[] {
			ENUMDP_TYP_ESG_AB,
			ENUMDP_TYP_ESG_AE,
			ENUMDP_TYP_ESG_AF,
			ENUMDP_TYP_ESG_AS_136,
			ENUMDP_TYP_ESG_AS_187,
			ENUMDP_TYP_ESG_AS_24,
			ENUMDP_TYP_ESG_AS_48,
			ENUMDP_TYP_ESG_AS_72,
			ENUMDP_TYP_ESG_AS_99,
			ENUMDP_TYP_ESG_AW,
			ENUMDP_TYP_ESG_BA,
			ENUMDP_TYP_ESG_BM,
			ENUMDP_TYP_ESG_BP,
			ENUMDP_TYP_ESG_BR,
			ENUMDP_TYP_ESG_BS,
			ENUMDP_TYP_ESG_FH1,
			ENUMDP_TYP_ESG_FH2,
			ENUMDP_TYP_ESG_FM1,
			ENUMDP_TYP_ESG_FM2,
			ENUMDP_TYP_ESG_GH1,
			ENUMDP_TYP_ESG_GH2,
			ENUMDP_TYP_ESG_GH3,
			ENUMDP_TYP_ESG_GH4,
			ENUMDP_TYP_ESG_GH5,
			ENUMDP_TYP_ESG_GM1,
			ENUMDP_TYP_ESG_GM2,
			ENUMDP_TYP_ESG_GM3,
			ENUMDP_TYP_ESG_GM4,
			ENUMDP_TYP_ESG_GM5,
			ENUMDP_TYP_ESG_HS,
			ENUMDP_TYP_ESG_LB,
			ENUMDP_TYP_ESG_LF,
			ENUMDP_TYP_ESG_LR,
			ENUMDP_TYP_ESG_MS,
			ENUMDP_TYP_ESG_NV,
			ENUMDP_TYP_ESG_RP1,
			ENUMDP_TYP_ESG_RP2,
			ENUMDP_TYP_ESG_RS,
			ENUMDP_TYP_ESG_SA,
			ENUMDP_TYP_ESG_SB,
			ENUMDP_TYP_ESG_SK,
			ENUMDP_TYP_ESG_SN,
			ENUMDP_TYP_ESG_SONSTIGE,
			ENUMDP_TYP_ESG_SP,
			ENUMDP_TYP_ESG_SS_136,
			ENUMDP_TYP_ESG_SS_187,
			ENUMDP_TYP_ESG_SS_24,
			ENUMDP_TYP_ESG_SS_48,
			ENUMDP_TYP_ESG_SS_72,
			ENUMDP_TYP_ESG_SS_99,
			ENUMDP_TYP_ESG_ST,
			ENUMDP_TYP_ESG_TA,
			ENUMDP_TYP_ESG_TE_E,
			ENUMDP_TYP_ESG_TE_L,
			ENUMDP_TYP_ESG_TE2,
			ENUMDP_TYP_ESG_TG,
			ENUMDP_TYP_ESG_TM_E,
			ENUMDP_TYP_ESG_TM_L,
			ENUMDP_TYP_ESG_TP,
			ENUMDP_TYP_ESG_TR,
			ENUMDP_TYP_ESG_TS,
			ENUMDP_TYP_ESG_TV_E,
			ENUMDP_TYP_ESG_TV_L,
			ENUMDP_TYP_ESG_VS,
			ENUMDP_TYP_ESG_VW,
		};

	/**
	 * A public read-only list of all the '<em><b>ENUMDP Typ ESG</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ENUMDPTypESG> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>ENUMDP Typ ESG</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENUMDPTypESG get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ENUMDPTypESG result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ENUMDP Typ ESG</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENUMDPTypESG getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ENUMDPTypESG result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ENUMDP Typ ESG</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENUMDPTypESG get(int value) {
		switch (value) {
			case ENUMDP_TYP_ESG_AB_VALUE: return ENUMDP_TYP_ESG_AB;
			case ENUMDP_TYP_ESG_AE_VALUE: return ENUMDP_TYP_ESG_AE;
			case ENUMDP_TYP_ESG_AF_VALUE: return ENUMDP_TYP_ESG_AF;
			case ENUMDP_TYP_ESG_AS_136_VALUE: return ENUMDP_TYP_ESG_AS_136;
			case ENUMDP_TYP_ESG_AS_187_VALUE: return ENUMDP_TYP_ESG_AS_187;
			case ENUMDP_TYP_ESG_AS_24_VALUE: return ENUMDP_TYP_ESG_AS_24;
			case ENUMDP_TYP_ESG_AS_48_VALUE: return ENUMDP_TYP_ESG_AS_48;
			case ENUMDP_TYP_ESG_AS_72_VALUE: return ENUMDP_TYP_ESG_AS_72;
			case ENUMDP_TYP_ESG_AS_99_VALUE: return ENUMDP_TYP_ESG_AS_99;
			case ENUMDP_TYP_ESG_AW_VALUE: return ENUMDP_TYP_ESG_AW;
			case ENUMDP_TYP_ESG_BA_VALUE: return ENUMDP_TYP_ESG_BA;
			case ENUMDP_TYP_ESG_BM_VALUE: return ENUMDP_TYP_ESG_BM;
			case ENUMDP_TYP_ESG_BP_VALUE: return ENUMDP_TYP_ESG_BP;
			case ENUMDP_TYP_ESG_BR_VALUE: return ENUMDP_TYP_ESG_BR;
			case ENUMDP_TYP_ESG_BS_VALUE: return ENUMDP_TYP_ESG_BS;
			case ENUMDP_TYP_ESG_FH1_VALUE: return ENUMDP_TYP_ESG_FH1;
			case ENUMDP_TYP_ESG_FH2_VALUE: return ENUMDP_TYP_ESG_FH2;
			case ENUMDP_TYP_ESG_FM1_VALUE: return ENUMDP_TYP_ESG_FM1;
			case ENUMDP_TYP_ESG_FM2_VALUE: return ENUMDP_TYP_ESG_FM2;
			case ENUMDP_TYP_ESG_GH1_VALUE: return ENUMDP_TYP_ESG_GH1;
			case ENUMDP_TYP_ESG_GH2_VALUE: return ENUMDP_TYP_ESG_GH2;
			case ENUMDP_TYP_ESG_GH3_VALUE: return ENUMDP_TYP_ESG_GH3;
			case ENUMDP_TYP_ESG_GH4_VALUE: return ENUMDP_TYP_ESG_GH4;
			case ENUMDP_TYP_ESG_GH5_VALUE: return ENUMDP_TYP_ESG_GH5;
			case ENUMDP_TYP_ESG_GM1_VALUE: return ENUMDP_TYP_ESG_GM1;
			case ENUMDP_TYP_ESG_GM2_VALUE: return ENUMDP_TYP_ESG_GM2;
			case ENUMDP_TYP_ESG_GM3_VALUE: return ENUMDP_TYP_ESG_GM3;
			case ENUMDP_TYP_ESG_GM4_VALUE: return ENUMDP_TYP_ESG_GM4;
			case ENUMDP_TYP_ESG_GM5_VALUE: return ENUMDP_TYP_ESG_GM5;
			case ENUMDP_TYP_ESG_HS_VALUE: return ENUMDP_TYP_ESG_HS;
			case ENUMDP_TYP_ESG_LB_VALUE: return ENUMDP_TYP_ESG_LB;
			case ENUMDP_TYP_ESG_LF_VALUE: return ENUMDP_TYP_ESG_LF;
			case ENUMDP_TYP_ESG_LR_VALUE: return ENUMDP_TYP_ESG_LR;
			case ENUMDP_TYP_ESG_MS_VALUE: return ENUMDP_TYP_ESG_MS;
			case ENUMDP_TYP_ESG_NV_VALUE: return ENUMDP_TYP_ESG_NV;
			case ENUMDP_TYP_ESG_RP1_VALUE: return ENUMDP_TYP_ESG_RP1;
			case ENUMDP_TYP_ESG_RP2_VALUE: return ENUMDP_TYP_ESG_RP2;
			case ENUMDP_TYP_ESG_RS_VALUE: return ENUMDP_TYP_ESG_RS;
			case ENUMDP_TYP_ESG_SA_VALUE: return ENUMDP_TYP_ESG_SA;
			case ENUMDP_TYP_ESG_SB_VALUE: return ENUMDP_TYP_ESG_SB;
			case ENUMDP_TYP_ESG_SK_VALUE: return ENUMDP_TYP_ESG_SK;
			case ENUMDP_TYP_ESG_SN_VALUE: return ENUMDP_TYP_ESG_SN;
			case ENUMDP_TYP_ESG_SONSTIGE_VALUE: return ENUMDP_TYP_ESG_SONSTIGE;
			case ENUMDP_TYP_ESG_SP_VALUE: return ENUMDP_TYP_ESG_SP;
			case ENUMDP_TYP_ESG_SS_136_VALUE: return ENUMDP_TYP_ESG_SS_136;
			case ENUMDP_TYP_ESG_SS_187_VALUE: return ENUMDP_TYP_ESG_SS_187;
			case ENUMDP_TYP_ESG_SS_24_VALUE: return ENUMDP_TYP_ESG_SS_24;
			case ENUMDP_TYP_ESG_SS_48_VALUE: return ENUMDP_TYP_ESG_SS_48;
			case ENUMDP_TYP_ESG_SS_72_VALUE: return ENUMDP_TYP_ESG_SS_72;
			case ENUMDP_TYP_ESG_SS_99_VALUE: return ENUMDP_TYP_ESG_SS_99;
			case ENUMDP_TYP_ESG_ST_VALUE: return ENUMDP_TYP_ESG_ST;
			case ENUMDP_TYP_ESG_TA_VALUE: return ENUMDP_TYP_ESG_TA;
			case ENUMDP_TYP_ESG_TE_E_VALUE: return ENUMDP_TYP_ESG_TE_E;
			case ENUMDP_TYP_ESG_TE_L_VALUE: return ENUMDP_TYP_ESG_TE_L;
			case ENUMDP_TYP_ESG_TE2_VALUE: return ENUMDP_TYP_ESG_TE2;
			case ENUMDP_TYP_ESG_TG_VALUE: return ENUMDP_TYP_ESG_TG;
			case ENUMDP_TYP_ESG_TM_E_VALUE: return ENUMDP_TYP_ESG_TM_E;
			case ENUMDP_TYP_ESG_TM_L_VALUE: return ENUMDP_TYP_ESG_TM_L;
			case ENUMDP_TYP_ESG_TP_VALUE: return ENUMDP_TYP_ESG_TP;
			case ENUMDP_TYP_ESG_TR_VALUE: return ENUMDP_TYP_ESG_TR;
			case ENUMDP_TYP_ESG_TS_VALUE: return ENUMDP_TYP_ESG_TS;
			case ENUMDP_TYP_ESG_TV_E_VALUE: return ENUMDP_TYP_ESG_TV_E;
			case ENUMDP_TYP_ESG_TV_L_VALUE: return ENUMDP_TYP_ESG_TV_L;
			case ENUMDP_TYP_ESG_VS_VALUE: return ENUMDP_TYP_ESG_VS;
			case ENUMDP_TYP_ESG_VW_VALUE: return ENUMDP_TYP_ESG_VW;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ENUMDPTypESG(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ENUMDPTypESG
