/**
 * Copyright (c) 2022 DB Netz AG and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package org.eclipse.set.model.model1902.Basisobjekte;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>ENUM Objektzustand Besonders</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.set.model.model1902.Basisobjekte.BasisobjektePackage#getENUMObjektzustandBesonders()
 * @model extendedMetaData="name='ENUMObjektzustand_Besonders'"
 * @generated
 */
public enum ENUMObjektzustandBesonders implements Enumerator {
	/**
	 * The '<em><b>ENUM Objektzustand Besonders abgedeckt</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_OBJEKTZUSTAND_BESONDERS_ABGEDECKT_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_OBJEKTZUSTAND_BESONDERS_ABGEDECKT(0, "ENUMObjektzustand_Besonders_abgedeckt", "abgedeckt"),

	/**
	 * The '<em><b>ENUM Objektzustand Besonders ausser Betrieb</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_OBJEKTZUSTAND_BESONDERS_AUSSER_BETRIEB_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_OBJEKTZUSTAND_BESONDERS_AUSSER_BETRIEB(1, "ENUMObjektzustand_Besonders_ausser_Betrieb", "ausser_Betrieb"),

	/**
	 * The '<em><b>ENUM Objektzustand Besonders ausser Betrieb vorbereitend</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_OBJEKTZUSTAND_BESONDERS_AUSSER_BETRIEB_VORBEREITEND_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_OBJEKTZUSTAND_BESONDERS_AUSSER_BETRIEB_VORBEREITEND(2, "ENUMObjektzustand_Besonders_ausser_Betrieb_vorbereitend", "ausser_Betrieb_vorbereitend"),

	/**
	 * The '<em><b>ENUM Objektzustand Besonders sonstige</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_OBJEKTZUSTAND_BESONDERS_SONSTIGE_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_OBJEKTZUSTAND_BESONDERS_SONSTIGE(3, "ENUMObjektzustand_Besonders_sonstige", "sonstige");

	/**
	 * The '<em><b>ENUM Objektzustand Besonders abgedeckt</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_OBJEKTZUSTAND_BESONDERS_ABGEDECKT
	 * @model name="ENUMObjektzustand_Besonders_abgedeckt" literal="abgedeckt"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_OBJEKTZUSTAND_BESONDERS_ABGEDECKT_VALUE = 0;

	/**
	 * The '<em><b>ENUM Objektzustand Besonders ausser Betrieb</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_OBJEKTZUSTAND_BESONDERS_AUSSER_BETRIEB
	 * @model name="ENUMObjektzustand_Besonders_ausser_Betrieb" literal="ausser_Betrieb"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_OBJEKTZUSTAND_BESONDERS_AUSSER_BETRIEB_VALUE = 1;

	/**
	 * The '<em><b>ENUM Objektzustand Besonders ausser Betrieb vorbereitend</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_OBJEKTZUSTAND_BESONDERS_AUSSER_BETRIEB_VORBEREITEND
	 * @model name="ENUMObjektzustand_Besonders_ausser_Betrieb_vorbereitend" literal="ausser_Betrieb_vorbereitend"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_OBJEKTZUSTAND_BESONDERS_AUSSER_BETRIEB_VORBEREITEND_VALUE = 2;

	/**
	 * The '<em><b>ENUM Objektzustand Besonders sonstige</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_OBJEKTZUSTAND_BESONDERS_SONSTIGE
	 * @model name="ENUMObjektzustand_Besonders_sonstige" literal="sonstige"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_OBJEKTZUSTAND_BESONDERS_SONSTIGE_VALUE = 3;

	/**
	 * An array of all the '<em><b>ENUM Objektzustand Besonders</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ENUMObjektzustandBesonders[] VALUES_ARRAY =
		new ENUMObjektzustandBesonders[] {
			ENUM_OBJEKTZUSTAND_BESONDERS_ABGEDECKT,
			ENUM_OBJEKTZUSTAND_BESONDERS_AUSSER_BETRIEB,
			ENUM_OBJEKTZUSTAND_BESONDERS_AUSSER_BETRIEB_VORBEREITEND,
			ENUM_OBJEKTZUSTAND_BESONDERS_SONSTIGE,
		};

	/**
	 * A public read-only list of all the '<em><b>ENUM Objektzustand Besonders</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ENUMObjektzustandBesonders> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>ENUM Objektzustand Besonders</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENUMObjektzustandBesonders get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ENUMObjektzustandBesonders result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ENUM Objektzustand Besonders</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENUMObjektzustandBesonders getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ENUMObjektzustandBesonders result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ENUM Objektzustand Besonders</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENUMObjektzustandBesonders get(int value) {
		switch (value) {
			case ENUM_OBJEKTZUSTAND_BESONDERS_ABGEDECKT_VALUE: return ENUM_OBJEKTZUSTAND_BESONDERS_ABGEDECKT;
			case ENUM_OBJEKTZUSTAND_BESONDERS_AUSSER_BETRIEB_VALUE: return ENUM_OBJEKTZUSTAND_BESONDERS_AUSSER_BETRIEB;
			case ENUM_OBJEKTZUSTAND_BESONDERS_AUSSER_BETRIEB_VORBEREITEND_VALUE: return ENUM_OBJEKTZUSTAND_BESONDERS_AUSSER_BETRIEB_VORBEREITEND;
			case ENUM_OBJEKTZUSTAND_BESONDERS_SONSTIGE_VALUE: return ENUM_OBJEKTZUSTAND_BESONDERS_SONSTIGE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ENUMObjektzustandBesonders(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ENUMObjektzustandBesonders
