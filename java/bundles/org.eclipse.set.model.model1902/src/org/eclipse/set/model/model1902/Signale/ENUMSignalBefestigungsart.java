/**
 * Copyright (c) 2022 DB Netz AG and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package org.eclipse.set.model.model1902.Signale;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>ENUM Signal Befestigungsart</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.set.model.model1902.Signale.SignalePackage#getENUMSignalBefestigungsart()
 * @model extendedMetaData="name='ENUMSignal_Befestigungsart'"
 * @generated
 */
public enum ENUMSignalBefestigungsart implements Enumerator {
	/**
	 * The '<em><b>ENUM Signal Befestigungsart Bahnsteig</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_BAHNSTEIG_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_SIGNAL_BEFESTIGUNGSART_BAHNSTEIG(0, "ENUMSignal_Befestigungsart_Bahnsteig", "Bahnsteig"),

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Fundament</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_FUNDAMENT_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_SIGNAL_BEFESTIGUNGSART_FUNDAMENT(1, "ENUMSignal_Befestigungsart_Fundament", "Fundament"),

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Konstruktionsteil</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_KONSTRUKTIONSTEIL_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_SIGNAL_BEFESTIGUNGSART_KONSTRUKTIONSTEIL(2, "ENUMSignal_Befestigungsart_Konstruktionsteil", "Konstruktionsteil"),

	/**
	 * The '<em><b>ENUM Signal Befestigungsart OL Kettenwerk</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_OL_KETTENWERK_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_SIGNAL_BEFESTIGUNGSART_OL_KETTENWERK(3, "ENUMSignal_Befestigungsart_OL_Kettenwerk", "OL_Kettenwerk"),

	/**
	 * The '<em><b>ENUM Signal Befestigungsart OL Mast</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_OL_MAST_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_SIGNAL_BEFESTIGUNGSART_OL_MAST(4, "ENUMSignal_Befestigungsart_OL_Mast", "OL_Mast"),

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Prellbock</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_PRELLBOCK_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_SIGNAL_BEFESTIGUNGSART_PRELLBOCK(5, "ENUMSignal_Befestigungsart_Prellbock", "Prellbock"),

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Signalausleger</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_SIGNALAUSLEGER_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_SIGNAL_BEFESTIGUNGSART_SIGNALAUSLEGER(6, "ENUMSignal_Befestigungsart_Signalausleger", "Signalausleger"),

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Signalbruecke</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_SIGNALBRUECKE_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_SIGNAL_BEFESTIGUNGSART_SIGNALBRUECKE(7, "ENUMSignal_Befestigungsart_Signalbruecke", "Signalbruecke"),

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Sonderkonstruktion</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_SONDERKONSTRUKTION_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_SIGNAL_BEFESTIGUNGSART_SONDERKONSTRUKTION(8, "ENUMSignal_Befestigungsart_Sonderkonstruktion", "Sonderkonstruktion"),

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Dach</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_DACH_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_SIGNAL_BEFESTIGUNGSART_DACH(9, "ENUMSignal_Befestigungsart_Dach", "Dach"),

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Mast</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_MAST_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_SIGNAL_BEFESTIGUNGSART_MAST(10, "ENUMSignal_Befestigungsart_Mast", "Mast"),

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Pfahl</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_PFAHL_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_SIGNAL_BEFESTIGUNGSART_PFAHL(11, "ENUMSignal_Befestigungsart_Pfahl", "Pfahl"),

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Wand</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_WAND_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_SIGNAL_BEFESTIGUNGSART_WAND(12, "ENUMSignal_Befestigungsart_Wand", "Wand");

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Bahnsteig</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_BAHNSTEIG
	 * @model name="ENUMSignal_Befestigungsart_Bahnsteig" literal="Bahnsteig"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_SIGNAL_BEFESTIGUNGSART_BAHNSTEIG_VALUE = 0;

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Fundament</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_FUNDAMENT
	 * @model name="ENUMSignal_Befestigungsart_Fundament" literal="Fundament"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_SIGNAL_BEFESTIGUNGSART_FUNDAMENT_VALUE = 1;

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Konstruktionsteil</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_KONSTRUKTIONSTEIL
	 * @model name="ENUMSignal_Befestigungsart_Konstruktionsteil" literal="Konstruktionsteil"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_SIGNAL_BEFESTIGUNGSART_KONSTRUKTIONSTEIL_VALUE = 2;

	/**
	 * The '<em><b>ENUM Signal Befestigungsart OL Kettenwerk</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_OL_KETTENWERK
	 * @model name="ENUMSignal_Befestigungsart_OL_Kettenwerk" literal="OL_Kettenwerk"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_SIGNAL_BEFESTIGUNGSART_OL_KETTENWERK_VALUE = 3;

	/**
	 * The '<em><b>ENUM Signal Befestigungsart OL Mast</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_OL_MAST
	 * @model name="ENUMSignal_Befestigungsart_OL_Mast" literal="OL_Mast"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_SIGNAL_BEFESTIGUNGSART_OL_MAST_VALUE = 4;

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Prellbock</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_PRELLBOCK
	 * @model name="ENUMSignal_Befestigungsart_Prellbock" literal="Prellbock"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_SIGNAL_BEFESTIGUNGSART_PRELLBOCK_VALUE = 5;

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Signalausleger</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_SIGNALAUSLEGER
	 * @model name="ENUMSignal_Befestigungsart_Signalausleger" literal="Signalausleger"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_SIGNAL_BEFESTIGUNGSART_SIGNALAUSLEGER_VALUE = 6;

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Signalbruecke</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_SIGNALBRUECKE
	 * @model name="ENUMSignal_Befestigungsart_Signalbruecke" literal="Signalbruecke"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_SIGNAL_BEFESTIGUNGSART_SIGNALBRUECKE_VALUE = 7;

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Sonderkonstruktion</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_SONDERKONSTRUKTION
	 * @model name="ENUMSignal_Befestigungsart_Sonderkonstruktion" literal="Sonderkonstruktion"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_SIGNAL_BEFESTIGUNGSART_SONDERKONSTRUKTION_VALUE = 8;

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Dach</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_DACH
	 * @model name="ENUMSignal_Befestigungsart_Dach" literal="Dach"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_SIGNAL_BEFESTIGUNGSART_DACH_VALUE = 9;

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Mast</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_MAST
	 * @model name="ENUMSignal_Befestigungsart_Mast" literal="Mast"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_SIGNAL_BEFESTIGUNGSART_MAST_VALUE = 10;

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Pfahl</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_PFAHL
	 * @model name="ENUMSignal_Befestigungsart_Pfahl" literal="Pfahl"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_SIGNAL_BEFESTIGUNGSART_PFAHL_VALUE = 11;

	/**
	 * The '<em><b>ENUM Signal Befestigungsart Wand</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_SIGNAL_BEFESTIGUNGSART_WAND
	 * @model name="ENUMSignal_Befestigungsart_Wand" literal="Wand"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_SIGNAL_BEFESTIGUNGSART_WAND_VALUE = 12;

	/**
	 * An array of all the '<em><b>ENUM Signal Befestigungsart</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ENUMSignalBefestigungsart[] VALUES_ARRAY =
		new ENUMSignalBefestigungsart[] {
			ENUM_SIGNAL_BEFESTIGUNGSART_BAHNSTEIG,
			ENUM_SIGNAL_BEFESTIGUNGSART_FUNDAMENT,
			ENUM_SIGNAL_BEFESTIGUNGSART_KONSTRUKTIONSTEIL,
			ENUM_SIGNAL_BEFESTIGUNGSART_OL_KETTENWERK,
			ENUM_SIGNAL_BEFESTIGUNGSART_OL_MAST,
			ENUM_SIGNAL_BEFESTIGUNGSART_PRELLBOCK,
			ENUM_SIGNAL_BEFESTIGUNGSART_SIGNALAUSLEGER,
			ENUM_SIGNAL_BEFESTIGUNGSART_SIGNALBRUECKE,
			ENUM_SIGNAL_BEFESTIGUNGSART_SONDERKONSTRUKTION,
			ENUM_SIGNAL_BEFESTIGUNGSART_DACH,
			ENUM_SIGNAL_BEFESTIGUNGSART_MAST,
			ENUM_SIGNAL_BEFESTIGUNGSART_PFAHL,
			ENUM_SIGNAL_BEFESTIGUNGSART_WAND,
		};

	/**
	 * A public read-only list of all the '<em><b>ENUM Signal Befestigungsart</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ENUMSignalBefestigungsart> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>ENUM Signal Befestigungsart</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENUMSignalBefestigungsart get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ENUMSignalBefestigungsart result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ENUM Signal Befestigungsart</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENUMSignalBefestigungsart getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ENUMSignalBefestigungsart result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ENUM Signal Befestigungsart</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENUMSignalBefestigungsart get(int value) {
		switch (value) {
			case ENUM_SIGNAL_BEFESTIGUNGSART_BAHNSTEIG_VALUE: return ENUM_SIGNAL_BEFESTIGUNGSART_BAHNSTEIG;
			case ENUM_SIGNAL_BEFESTIGUNGSART_FUNDAMENT_VALUE: return ENUM_SIGNAL_BEFESTIGUNGSART_FUNDAMENT;
			case ENUM_SIGNAL_BEFESTIGUNGSART_KONSTRUKTIONSTEIL_VALUE: return ENUM_SIGNAL_BEFESTIGUNGSART_KONSTRUKTIONSTEIL;
			case ENUM_SIGNAL_BEFESTIGUNGSART_OL_KETTENWERK_VALUE: return ENUM_SIGNAL_BEFESTIGUNGSART_OL_KETTENWERK;
			case ENUM_SIGNAL_BEFESTIGUNGSART_OL_MAST_VALUE: return ENUM_SIGNAL_BEFESTIGUNGSART_OL_MAST;
			case ENUM_SIGNAL_BEFESTIGUNGSART_PRELLBOCK_VALUE: return ENUM_SIGNAL_BEFESTIGUNGSART_PRELLBOCK;
			case ENUM_SIGNAL_BEFESTIGUNGSART_SIGNALAUSLEGER_VALUE: return ENUM_SIGNAL_BEFESTIGUNGSART_SIGNALAUSLEGER;
			case ENUM_SIGNAL_BEFESTIGUNGSART_SIGNALBRUECKE_VALUE: return ENUM_SIGNAL_BEFESTIGUNGSART_SIGNALBRUECKE;
			case ENUM_SIGNAL_BEFESTIGUNGSART_SONDERKONSTRUKTION_VALUE: return ENUM_SIGNAL_BEFESTIGUNGSART_SONDERKONSTRUKTION;
			case ENUM_SIGNAL_BEFESTIGUNGSART_DACH_VALUE: return ENUM_SIGNAL_BEFESTIGUNGSART_DACH;
			case ENUM_SIGNAL_BEFESTIGUNGSART_MAST_VALUE: return ENUM_SIGNAL_BEFESTIGUNGSART_MAST;
			case ENUM_SIGNAL_BEFESTIGUNGSART_PFAHL_VALUE: return ENUM_SIGNAL_BEFESTIGUNGSART_PFAHL;
			case ENUM_SIGNAL_BEFESTIGUNGSART_WAND_VALUE: return ENUM_SIGNAL_BEFESTIGUNGSART_WAND;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ENUMSignalBefestigungsart(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ENUMSignalBefestigungsart
