/**
 * Copyright (c) 2022 DB Netz AG and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package org.eclipse.set.model.model1902.Fahrstrasse;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>ENUM Fstr Art</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.set.model.model1902.Fahrstrasse.FahrstrassePackage#getENUMFstrArt()
 * @model extendedMetaData="name='ENUMFstr_Art'"
 * @generated
 */
public enum ENUMFstrArt implements Enumerator {
	/**
	 * The '<em><b>ENUM Fstr Art RR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_RR_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_FSTR_ART_RR(0, "ENUMFstr_Art_RR", "RR"),

	/**
	 * The '<em><b>ENUM Fstr Art ZZ</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_ZZ_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_FSTR_ART_ZZ(1, "ENUMFstr_Art_ZZ", "ZZ"),

	/**
	 * The '<em><b>ENUM Fstr Art RU</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_RU_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_FSTR_ART_RU(2, "ENUMFstr_Art_RU", "RU"),

	/**
	 * The '<em><b>ENUM Fstr Art ZH</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_ZH_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_FSTR_ART_ZH(3, "ENUMFstr_Art_ZH", "ZH"),

	/**
	 * The '<em><b>ENUM Fstr Art ZM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_ZM_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_FSTR_ART_ZM(4, "ENUMFstr_Art_ZM", "ZM"),

	/**
	 * The '<em><b>ENUM Fstr Art ZR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_ZR_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_FSTR_ART_ZR(5, "ENUMFstr_Art_ZR", "ZR"),

	/**
	 * The '<em><b>ENUM Fstr Art ZU</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_ZU_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_FSTR_ART_ZU(6, "ENUMFstr_Art_ZU", "ZU"),

	/**
	 * The '<em><b>ENUM Fstr Art ZUH</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_ZUH_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_FSTR_ART_ZUH(7, "ENUMFstr_Art_ZUH", "ZUH"),

	/**
	 * The '<em><b>ENUM Fstr Art ZUM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_ZUM_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_FSTR_ART_ZUM(8, "ENUMFstr_Art_ZUM", "ZUM"),

	/**
	 * The '<em><b>ENUM Fstr Art RT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_RT_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_FSTR_ART_RT(9, "ENUMFstr_Art_RT", "RT"),

	/**
	 * The '<em><b>ENUM Fstr Art ZT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_ZT_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_FSTR_ART_ZT(10, "ENUMFstr_Art_ZT", "ZT"),

	/**
	 * The '<em><b>ENUM Fstr Art RTU</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_RTU_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_FSTR_ART_RTU(11, "ENUMFstr_Art_RTU", "RTU"),

	/**
	 * The '<em><b>ENUM Fstr Art ZTU</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_ZTU_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_FSTR_ART_ZTU(12, "ENUMFstr_Art_ZTU", "ZTU");

	/**
	 * The '<em><b>ENUM Fstr Art RR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_RR
	 * @model name="ENUMFstr_Art_RR" literal="RR"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_FSTR_ART_RR_VALUE = 0;

	/**
	 * The '<em><b>ENUM Fstr Art ZZ</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_ZZ
	 * @model name="ENUMFstr_Art_ZZ" literal="ZZ"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_FSTR_ART_ZZ_VALUE = 1;

	/**
	 * The '<em><b>ENUM Fstr Art RU</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_RU
	 * @model name="ENUMFstr_Art_RU" literal="RU"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_FSTR_ART_RU_VALUE = 2;

	/**
	 * The '<em><b>ENUM Fstr Art ZH</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_ZH
	 * @model name="ENUMFstr_Art_ZH" literal="ZH"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_FSTR_ART_ZH_VALUE = 3;

	/**
	 * The '<em><b>ENUM Fstr Art ZM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_ZM
	 * @model name="ENUMFstr_Art_ZM" literal="ZM"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_FSTR_ART_ZM_VALUE = 4;

	/**
	 * The '<em><b>ENUM Fstr Art ZR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_ZR
	 * @model name="ENUMFstr_Art_ZR" literal="ZR"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_FSTR_ART_ZR_VALUE = 5;

	/**
	 * The '<em><b>ENUM Fstr Art ZU</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_ZU
	 * @model name="ENUMFstr_Art_ZU" literal="ZU"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_FSTR_ART_ZU_VALUE = 6;

	/**
	 * The '<em><b>ENUM Fstr Art ZUH</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_ZUH
	 * @model name="ENUMFstr_Art_ZUH" literal="ZUH"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_FSTR_ART_ZUH_VALUE = 7;

	/**
	 * The '<em><b>ENUM Fstr Art ZUM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_ZUM
	 * @model name="ENUMFstr_Art_ZUM" literal="ZUM"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_FSTR_ART_ZUM_VALUE = 8;

	/**
	 * The '<em><b>ENUM Fstr Art RT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_RT
	 * @model name="ENUMFstr_Art_RT" literal="RT"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_FSTR_ART_RT_VALUE = 9;

	/**
	 * The '<em><b>ENUM Fstr Art ZT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_ZT
	 * @model name="ENUMFstr_Art_ZT" literal="ZT"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_FSTR_ART_ZT_VALUE = 10;

	/**
	 * The '<em><b>ENUM Fstr Art RTU</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_RTU
	 * @model name="ENUMFstr_Art_RTU" literal="RTU"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_FSTR_ART_RTU_VALUE = 11;

	/**
	 * The '<em><b>ENUM Fstr Art ZTU</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_FSTR_ART_ZTU
	 * @model name="ENUMFstr_Art_ZTU" literal="ZTU"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_FSTR_ART_ZTU_VALUE = 12;

	/**
	 * An array of all the '<em><b>ENUM Fstr Art</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ENUMFstrArt[] VALUES_ARRAY =
		new ENUMFstrArt[] {
			ENUM_FSTR_ART_RR,
			ENUM_FSTR_ART_ZZ,
			ENUM_FSTR_ART_RU,
			ENUM_FSTR_ART_ZH,
			ENUM_FSTR_ART_ZM,
			ENUM_FSTR_ART_ZR,
			ENUM_FSTR_ART_ZU,
			ENUM_FSTR_ART_ZUH,
			ENUM_FSTR_ART_ZUM,
			ENUM_FSTR_ART_RT,
			ENUM_FSTR_ART_ZT,
			ENUM_FSTR_ART_RTU,
			ENUM_FSTR_ART_ZTU,
		};

	/**
	 * A public read-only list of all the '<em><b>ENUM Fstr Art</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ENUMFstrArt> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>ENUM Fstr Art</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENUMFstrArt get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ENUMFstrArt result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ENUM Fstr Art</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENUMFstrArt getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ENUMFstrArt result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ENUM Fstr Art</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENUMFstrArt get(int value) {
		switch (value) {
			case ENUM_FSTR_ART_RR_VALUE: return ENUM_FSTR_ART_RR;
			case ENUM_FSTR_ART_ZZ_VALUE: return ENUM_FSTR_ART_ZZ;
			case ENUM_FSTR_ART_RU_VALUE: return ENUM_FSTR_ART_RU;
			case ENUM_FSTR_ART_ZH_VALUE: return ENUM_FSTR_ART_ZH;
			case ENUM_FSTR_ART_ZM_VALUE: return ENUM_FSTR_ART_ZM;
			case ENUM_FSTR_ART_ZR_VALUE: return ENUM_FSTR_ART_ZR;
			case ENUM_FSTR_ART_ZU_VALUE: return ENUM_FSTR_ART_ZU;
			case ENUM_FSTR_ART_ZUH_VALUE: return ENUM_FSTR_ART_ZUH;
			case ENUM_FSTR_ART_ZUM_VALUE: return ENUM_FSTR_ART_ZUM;
			case ENUM_FSTR_ART_RT_VALUE: return ENUM_FSTR_ART_RT;
			case ENUM_FSTR_ART_ZT_VALUE: return ENUM_FSTR_ART_ZT;
			case ENUM_FSTR_ART_RTU_VALUE: return ENUM_FSTR_ART_RTU;
			case ENUM_FSTR_ART_ZTU_VALUE: return ENUM_FSTR_ART_ZTU;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ENUMFstrArt(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ENUMFstrArt
