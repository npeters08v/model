/**
 * Copyright (c) 2022 DB Netz AG and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package org.eclipse.set.model.model1902.Fahrstrasse;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fstr DWeg WKr Allg child Attribute Group</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.model.model1902.Fahrstrasse.FahrstrassePackage#getFstr_DWeg_W_Kr_Allg_child_AttributeGroup()
 * @model extendedMetaData="name='CFstr_DWeg_W_Kr_Allg_child' kind='elementOnly'"
 * @generated
 */
public interface Fstr_DWeg_W_Kr_Allg_child_AttributeGroup extends Fstr_DWeg_W_Kr_Allg_AttributeGroup {
} // Fstr_DWeg_W_Kr_Allg_child_AttributeGroup
