/**
 * Copyright (c) 2022 DB Netz AG and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package org.eclipse.set.model.model1902.Fahrstrasse.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.set.model.model1902.Fahrstrasse.FahrstrassePackage;
import org.eclipse.set.model.model1902.Fahrstrasse.Fstr_DWeg_W_Kr_Allg_child_AttributeGroup;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fstr DWeg WKr Allg child Attribute Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Fstr_DWeg_W_Kr_Allg_child_AttributeGroupImpl extends Fstr_DWeg_W_Kr_Allg_AttributeGroupImpl implements Fstr_DWeg_W_Kr_Allg_child_AttributeGroup {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Fstr_DWeg_W_Kr_Allg_child_AttributeGroupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FahrstrassePackage.Literals.FSTR_DWEG_WKR_ALLG_CHILD_ATTRIBUTE_GROUP;
	}

} //Fstr_DWeg_W_Kr_Allg_child_AttributeGroupImpl
