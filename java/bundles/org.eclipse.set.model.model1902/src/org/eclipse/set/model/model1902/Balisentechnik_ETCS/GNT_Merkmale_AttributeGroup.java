/**
 * Copyright (c) 2022 DB Netz AG and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package org.eclipse.set.model.model1902.Balisentechnik_ETCS;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>GNT Merkmale Attribute Group</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.model.model1902.Balisentechnik_ETCS.Balisentechnik_ETCSPackage#getGNT_Merkmale_AttributeGroup()
 * @model extendedMetaData="name='CGNT_Merkmale' kind='empty'"
 * @generated
 */
public interface GNT_Merkmale_AttributeGroup extends EObject {
} // GNT_Merkmale_AttributeGroup
