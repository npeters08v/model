/**
 * Copyright (c) 2022 DB Netz AG and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package org.eclipse.set.model.model1902.Balisentechnik_ETCS;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ZUB Bereichsgrenze Nach GNT Attribute Group</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.model.model1902.Balisentechnik_ETCS.Balisentechnik_ETCSPackage#getZUB_Bereichsgrenze_Nach_GNT_AttributeGroup()
 * @model extendedMetaData="name='CZUB_Bereichsgrenze_Nach_GNT' kind='empty'"
 * @generated
 */
public interface ZUB_Bereichsgrenze_Nach_GNT_AttributeGroup extends EObject {
} // ZUB_Bereichsgrenze_Nach_GNT_AttributeGroup
