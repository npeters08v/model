/**
 * Copyright (c) 2022 DB Netz AG and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package org.eclipse.set.model.model1902.Bahnuebergang;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>ENUMBUE Sicherungsart</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.set.model.model1902.Bahnuebergang.BahnuebergangPackage#getENUMBUESicherungsart()
 * @model extendedMetaData="name='ENUMBUE_Sicherungsart'"
 * @generated
 */
public enum ENUMBUESicherungsart implements Enumerator {
	/**
	 * The '<em><b>ENUMBUE Sicherungsart Aund Lf</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_AUND_LF_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMBUE_SICHERUNGSART_AUND_LF(0, "ENUMBUE_Sicherungsart_A_und_Lf", "A_und_Lf"),

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Aund Sprechverbindung</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_AUND_SPRECHVERBINDUNG_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMBUE_SICHERUNGSART_AUND_SPRECHVERBINDUNG(1, "ENUMBUE_Sicherungsart_A_und_Sprechverbindung", "A_und_Sprechverbindung"),

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Lz</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_LZ_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMBUE_SICHERUNGSART_LZ(2, "ENUMBUE_Sicherungsart_Lz", "Lz"),

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Lz H</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_LZ_H_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMBUE_SICHERUNGSART_LZ_H(3, "ENUMBUE_Sicherungsart_LzH", "LzH"),

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Lz H2F</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_LZ_H2F_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMBUE_SICHERUNGSART_LZ_H2F(4, "ENUMBUE_Sicherungsart_LzH_2F", "LzH_2F"),

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Lz HF</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_LZ_HF_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMBUE_SICHERUNGSART_LZ_HF(5, "ENUMBUE_Sicherungsart_LzH_F", "LzH_F"),

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Lz HH</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_LZ_HH_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMBUE_SICHERUNGSART_LZ_HH(6, "ENUMBUE_Sicherungsart_LzHH", "LzHH"),

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Lz V</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_LZ_V_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMBUE_SICHERUNGSART_LZ_V(7, "ENUMBUE_Sicherungsart_LzV", "LzV"),

	/**
	 * The '<em><b>ENUMBUE Sicherungsart P</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_P_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMBUE_SICHERUNGSART_P(8, "ENUMBUE_Sicherungsart_P", "P"),

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Pund Lf</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_PUND_LF_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMBUE_SICHERUNGSART_PUND_LF(9, "ENUMBUE_Sicherungsart_P_und_Lf", "P_und_Lf"),

	/**
	 * The '<em><b>ENUMBUE Sicherungsart schluesselabhaengig</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_SCHLUESSELABHAENGIG_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMBUE_SICHERUNGSART_SCHLUESSELABHAENGIG(10, "ENUMBUE_Sicherungsart_schluesselabhaengig", "schluesselabhaengig"),

	/**
	 * The '<em><b>ENUMBUE Sicherungsart sonstige</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_SONSTIGE_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMBUE_SICHERUNGSART_SONSTIGE(11, "ENUMBUE_Sicherungsart_sonstige", "sonstige"),

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Ue</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_UE_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMBUE_SICHERUNGSART_UE(12, "ENUMBUE_Sicherungsart_Ue", "Ue"),

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Ue und P</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_UE_UND_P_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMBUE_SICHERUNGSART_UE_UND_P(13, "ENUMBUE_Sicherungsart_Ue_und_P", "Ue_und_P");

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Aund Lf</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_AUND_LF
	 * @model name="ENUMBUE_Sicherungsart_A_und_Lf" literal="A_und_Lf"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMBUE_SICHERUNGSART_AUND_LF_VALUE = 0;

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Aund Sprechverbindung</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_AUND_SPRECHVERBINDUNG
	 * @model name="ENUMBUE_Sicherungsart_A_und_Sprechverbindung" literal="A_und_Sprechverbindung"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMBUE_SICHERUNGSART_AUND_SPRECHVERBINDUNG_VALUE = 1;

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Lz</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_LZ
	 * @model name="ENUMBUE_Sicherungsart_Lz" literal="Lz"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMBUE_SICHERUNGSART_LZ_VALUE = 2;

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Lz H</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_LZ_H
	 * @model name="ENUMBUE_Sicherungsart_LzH" literal="LzH"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMBUE_SICHERUNGSART_LZ_H_VALUE = 3;

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Lz H2F</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_LZ_H2F
	 * @model name="ENUMBUE_Sicherungsart_LzH_2F" literal="LzH_2F"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMBUE_SICHERUNGSART_LZ_H2F_VALUE = 4;

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Lz HF</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_LZ_HF
	 * @model name="ENUMBUE_Sicherungsart_LzH_F" literal="LzH_F"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMBUE_SICHERUNGSART_LZ_HF_VALUE = 5;

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Lz HH</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_LZ_HH
	 * @model name="ENUMBUE_Sicherungsart_LzHH" literal="LzHH"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMBUE_SICHERUNGSART_LZ_HH_VALUE = 6;

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Lz V</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_LZ_V
	 * @model name="ENUMBUE_Sicherungsart_LzV" literal="LzV"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMBUE_SICHERUNGSART_LZ_V_VALUE = 7;

	/**
	 * The '<em><b>ENUMBUE Sicherungsart P</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_P
	 * @model name="ENUMBUE_Sicherungsart_P" literal="P"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMBUE_SICHERUNGSART_P_VALUE = 8;

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Pund Lf</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_PUND_LF
	 * @model name="ENUMBUE_Sicherungsart_P_und_Lf" literal="P_und_Lf"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMBUE_SICHERUNGSART_PUND_LF_VALUE = 9;

	/**
	 * The '<em><b>ENUMBUE Sicherungsart schluesselabhaengig</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_SCHLUESSELABHAENGIG
	 * @model name="ENUMBUE_Sicherungsart_schluesselabhaengig" literal="schluesselabhaengig"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMBUE_SICHERUNGSART_SCHLUESSELABHAENGIG_VALUE = 10;

	/**
	 * The '<em><b>ENUMBUE Sicherungsart sonstige</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_SONSTIGE
	 * @model name="ENUMBUE_Sicherungsart_sonstige" literal="sonstige"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMBUE_SICHERUNGSART_SONSTIGE_VALUE = 11;

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Ue</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_UE
	 * @model name="ENUMBUE_Sicherungsart_Ue" literal="Ue"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMBUE_SICHERUNGSART_UE_VALUE = 12;

	/**
	 * The '<em><b>ENUMBUE Sicherungsart Ue und P</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUMBUE_SICHERUNGSART_UE_UND_P
	 * @model name="ENUMBUE_Sicherungsart_Ue_und_P" literal="Ue_und_P"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMBUE_SICHERUNGSART_UE_UND_P_VALUE = 13;

	/**
	 * An array of all the '<em><b>ENUMBUE Sicherungsart</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ENUMBUESicherungsart[] VALUES_ARRAY =
		new ENUMBUESicherungsart[] {
			ENUMBUE_SICHERUNGSART_AUND_LF,
			ENUMBUE_SICHERUNGSART_AUND_SPRECHVERBINDUNG,
			ENUMBUE_SICHERUNGSART_LZ,
			ENUMBUE_SICHERUNGSART_LZ_H,
			ENUMBUE_SICHERUNGSART_LZ_H2F,
			ENUMBUE_SICHERUNGSART_LZ_HF,
			ENUMBUE_SICHERUNGSART_LZ_HH,
			ENUMBUE_SICHERUNGSART_LZ_V,
			ENUMBUE_SICHERUNGSART_P,
			ENUMBUE_SICHERUNGSART_PUND_LF,
			ENUMBUE_SICHERUNGSART_SCHLUESSELABHAENGIG,
			ENUMBUE_SICHERUNGSART_SONSTIGE,
			ENUMBUE_SICHERUNGSART_UE,
			ENUMBUE_SICHERUNGSART_UE_UND_P,
		};

	/**
	 * A public read-only list of all the '<em><b>ENUMBUE Sicherungsart</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ENUMBUESicherungsart> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>ENUMBUE Sicherungsart</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENUMBUESicherungsart get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ENUMBUESicherungsart result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ENUMBUE Sicherungsart</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENUMBUESicherungsart getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ENUMBUESicherungsart result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ENUMBUE Sicherungsart</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENUMBUESicherungsart get(int value) {
		switch (value) {
			case ENUMBUE_SICHERUNGSART_AUND_LF_VALUE: return ENUMBUE_SICHERUNGSART_AUND_LF;
			case ENUMBUE_SICHERUNGSART_AUND_SPRECHVERBINDUNG_VALUE: return ENUMBUE_SICHERUNGSART_AUND_SPRECHVERBINDUNG;
			case ENUMBUE_SICHERUNGSART_LZ_VALUE: return ENUMBUE_SICHERUNGSART_LZ;
			case ENUMBUE_SICHERUNGSART_LZ_H_VALUE: return ENUMBUE_SICHERUNGSART_LZ_H;
			case ENUMBUE_SICHERUNGSART_LZ_H2F_VALUE: return ENUMBUE_SICHERUNGSART_LZ_H2F;
			case ENUMBUE_SICHERUNGSART_LZ_HF_VALUE: return ENUMBUE_SICHERUNGSART_LZ_HF;
			case ENUMBUE_SICHERUNGSART_LZ_HH_VALUE: return ENUMBUE_SICHERUNGSART_LZ_HH;
			case ENUMBUE_SICHERUNGSART_LZ_V_VALUE: return ENUMBUE_SICHERUNGSART_LZ_V;
			case ENUMBUE_SICHERUNGSART_P_VALUE: return ENUMBUE_SICHERUNGSART_P;
			case ENUMBUE_SICHERUNGSART_PUND_LF_VALUE: return ENUMBUE_SICHERUNGSART_PUND_LF;
			case ENUMBUE_SICHERUNGSART_SCHLUESSELABHAENGIG_VALUE: return ENUMBUE_SICHERUNGSART_SCHLUESSELABHAENGIG;
			case ENUMBUE_SICHERUNGSART_SONSTIGE_VALUE: return ENUMBUE_SICHERUNGSART_SONSTIGE;
			case ENUMBUE_SICHERUNGSART_UE_VALUE: return ENUMBUE_SICHERUNGSART_UE;
			case ENUMBUE_SICHERUNGSART_UE_UND_P_VALUE: return ENUMBUE_SICHERUNGSART_UE_UND_P;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ENUMBUESicherungsart(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ENUMBUESicherungsart
