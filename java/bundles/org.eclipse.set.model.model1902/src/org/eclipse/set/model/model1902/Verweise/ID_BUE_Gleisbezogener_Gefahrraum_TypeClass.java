/**
 * Copyright (c) 2022 DB Netz AG and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package org.eclipse.set.model.model1902.Verweise;

import org.eclipse.set.model.model1902.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID BUE Gleisbezogener Gefahrraum Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.model.model1902.Verweise.VerweisePackage#getID_BUE_Gleisbezogener_Gefahrraum_TypeClass()
 * @model extendedMetaData="name='TCID_BUE_Gleisbezogener_Gefahrraum' kind='elementOnly'"
 * @generated
 */
public interface ID_BUE_Gleisbezogener_Gefahrraum_TypeClass extends Zeiger_TypeClass {
} // ID_BUE_Gleisbezogener_Gefahrraum_TypeClass
