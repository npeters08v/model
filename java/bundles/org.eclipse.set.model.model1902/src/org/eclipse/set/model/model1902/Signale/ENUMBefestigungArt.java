/**
 * Copyright (c) 2022 DB Netz AG and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package org.eclipse.set.model.model1902.Signale;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>ENUM Befestigung Art</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.set.model.model1902.Signale.SignalePackage#getENUMBefestigungArt()
 * @model extendedMetaData="name='ENUMBefestigung_Art'"
 * @generated
 */
public enum ENUMBefestigungArt implements Enumerator {
	/**
	 * The '<em><b>ENUM Befestigung Art Fundament</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_FUNDAMENT_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_BEFESTIGUNG_ART_FUNDAMENT(0, "ENUMBefestigung_Art_Fundament", "Fundament"),

	/**
	 * The '<em><b>ENUM Befestigung Art Pfosten</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_PFOSTEN_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_BEFESTIGUNG_ART_PFOSTEN(1, "ENUMBefestigung_Art_Pfosten", "Pfosten"),

	/**
	 * The '<em><b>ENUM Befestigung Art Schienenfuss</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_SCHIENENFUSS_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_BEFESTIGUNG_ART_SCHIENENFUSS(2, "ENUMBefestigung_Art_Schienenfuss", "Schienenfuss"),

	/**
	 * The '<em><b>ENUM Befestigung Art Mast</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_MAST_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_BEFESTIGUNG_ART_MAST(3, "ENUMBefestigung_Art_Mast", "Mast"),

	/**
	 * The '<em><b>ENUM Befestigung Art Rahmen</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_RAHMEN_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_BEFESTIGUNG_ART_RAHMEN(4, "ENUMBefestigung_Art_Rahmen", "Rahmen"),

	/**
	 * The '<em><b>ENUM Befestigung Art Signal Anordnung Arbeitsbuehne</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_ARBEITSBUEHNE_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_ARBEITSBUEHNE(5, "ENUMBefestigung_Art_Signal_Anordnung_Arbeitsbuehne", "Signal_Anordnung_Arbeitsbuehne"),

	/**
	 * The '<em><b>ENUM Befestigung Art Signal Anordnung Mast</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_MAST_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_MAST(6, "ENUMBefestigung_Art_Signal_Anordnung_Mast", "Signal_Anordnung_Mast"),

	/**
	 * The '<em><b>ENUM Befestigung Art Signal Anordnung Sonstige</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_SONSTIGE_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_SONSTIGE(7, "ENUMBefestigung_Art_Signal_Anordnung_Sonstige", "Signal_Anordnung_Sonstige"),

	/**
	 * The '<em><b>ENUM Befestigung Art Signalausleger</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_SIGNALAUSLEGER_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_BEFESTIGUNG_ART_SIGNALAUSLEGER(8, "ENUMBefestigung_Art_Signalausleger", "Signalausleger"),

	/**
	 * The '<em><b>ENUM Befestigung Art Signalbruecke</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_SIGNALBRUECKE_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_BEFESTIGUNG_ART_SIGNALBRUECKE(9, "ENUMBefestigung_Art_Signalbruecke", "Signalbruecke"),

	/**
	 * The '<em><b>ENUM Befestigung Art sonstige</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_SONSTIGE_VALUE
	 * @generated
	 * @ordered
	 */
	ENUM_BEFESTIGUNG_ART_SONSTIGE(10, "ENUMBefestigung_Art_sonstige", "sonstige");

	/**
	 * The '<em><b>ENUM Befestigung Art Fundament</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_FUNDAMENT
	 * @model name="ENUMBefestigung_Art_Fundament" literal="Fundament"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_BEFESTIGUNG_ART_FUNDAMENT_VALUE = 0;

	/**
	 * The '<em><b>ENUM Befestigung Art Pfosten</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_PFOSTEN
	 * @model name="ENUMBefestigung_Art_Pfosten" literal="Pfosten"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_BEFESTIGUNG_ART_PFOSTEN_VALUE = 1;

	/**
	 * The '<em><b>ENUM Befestigung Art Schienenfuss</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_SCHIENENFUSS
	 * @model name="ENUMBefestigung_Art_Schienenfuss" literal="Schienenfuss"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_BEFESTIGUNG_ART_SCHIENENFUSS_VALUE = 2;

	/**
	 * The '<em><b>ENUM Befestigung Art Mast</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_MAST
	 * @model name="ENUMBefestigung_Art_Mast" literal="Mast"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_BEFESTIGUNG_ART_MAST_VALUE = 3;

	/**
	 * The '<em><b>ENUM Befestigung Art Rahmen</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_RAHMEN
	 * @model name="ENUMBefestigung_Art_Rahmen" literal="Rahmen"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_BEFESTIGUNG_ART_RAHMEN_VALUE = 4;

	/**
	 * The '<em><b>ENUM Befestigung Art Signal Anordnung Arbeitsbuehne</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_ARBEITSBUEHNE
	 * @model name="ENUMBefestigung_Art_Signal_Anordnung_Arbeitsbuehne" literal="Signal_Anordnung_Arbeitsbuehne"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_ARBEITSBUEHNE_VALUE = 5;

	/**
	 * The '<em><b>ENUM Befestigung Art Signal Anordnung Mast</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_MAST
	 * @model name="ENUMBefestigung_Art_Signal_Anordnung_Mast" literal="Signal_Anordnung_Mast"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_MAST_VALUE = 6;

	/**
	 * The '<em><b>ENUM Befestigung Art Signal Anordnung Sonstige</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_SONSTIGE
	 * @model name="ENUMBefestigung_Art_Signal_Anordnung_Sonstige" literal="Signal_Anordnung_Sonstige"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_SONSTIGE_VALUE = 7;

	/**
	 * The '<em><b>ENUM Befestigung Art Signalausleger</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_SIGNALAUSLEGER
	 * @model name="ENUMBefestigung_Art_Signalausleger" literal="Signalausleger"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_BEFESTIGUNG_ART_SIGNALAUSLEGER_VALUE = 8;

	/**
	 * The '<em><b>ENUM Befestigung Art Signalbruecke</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_SIGNALBRUECKE
	 * @model name="ENUMBefestigung_Art_Signalbruecke" literal="Signalbruecke"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_BEFESTIGUNG_ART_SIGNALBRUECKE_VALUE = 9;

	/**
	 * The '<em><b>ENUM Befestigung Art sonstige</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENUM_BEFESTIGUNG_ART_SONSTIGE
	 * @model name="ENUMBefestigung_Art_sonstige" literal="sonstige"
	 * @generated
	 * @ordered
	 */
	public static final int ENUM_BEFESTIGUNG_ART_SONSTIGE_VALUE = 10;

	/**
	 * An array of all the '<em><b>ENUM Befestigung Art</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ENUMBefestigungArt[] VALUES_ARRAY =
		new ENUMBefestigungArt[] {
			ENUM_BEFESTIGUNG_ART_FUNDAMENT,
			ENUM_BEFESTIGUNG_ART_PFOSTEN,
			ENUM_BEFESTIGUNG_ART_SCHIENENFUSS,
			ENUM_BEFESTIGUNG_ART_MAST,
			ENUM_BEFESTIGUNG_ART_RAHMEN,
			ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_ARBEITSBUEHNE,
			ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_MAST,
			ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_SONSTIGE,
			ENUM_BEFESTIGUNG_ART_SIGNALAUSLEGER,
			ENUM_BEFESTIGUNG_ART_SIGNALBRUECKE,
			ENUM_BEFESTIGUNG_ART_SONSTIGE,
		};

	/**
	 * A public read-only list of all the '<em><b>ENUM Befestigung Art</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ENUMBefestigungArt> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>ENUM Befestigung Art</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENUMBefestigungArt get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ENUMBefestigungArt result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ENUM Befestigung Art</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENUMBefestigungArt getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ENUMBefestigungArt result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ENUM Befestigung Art</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENUMBefestigungArt get(int value) {
		switch (value) {
			case ENUM_BEFESTIGUNG_ART_FUNDAMENT_VALUE: return ENUM_BEFESTIGUNG_ART_FUNDAMENT;
			case ENUM_BEFESTIGUNG_ART_PFOSTEN_VALUE: return ENUM_BEFESTIGUNG_ART_PFOSTEN;
			case ENUM_BEFESTIGUNG_ART_SCHIENENFUSS_VALUE: return ENUM_BEFESTIGUNG_ART_SCHIENENFUSS;
			case ENUM_BEFESTIGUNG_ART_MAST_VALUE: return ENUM_BEFESTIGUNG_ART_MAST;
			case ENUM_BEFESTIGUNG_ART_RAHMEN_VALUE: return ENUM_BEFESTIGUNG_ART_RAHMEN;
			case ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_ARBEITSBUEHNE_VALUE: return ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_ARBEITSBUEHNE;
			case ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_MAST_VALUE: return ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_MAST;
			case ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_SONSTIGE_VALUE: return ENUM_BEFESTIGUNG_ART_SIGNAL_ANORDNUNG_SONSTIGE;
			case ENUM_BEFESTIGUNG_ART_SIGNALAUSLEGER_VALUE: return ENUM_BEFESTIGUNG_ART_SIGNALAUSLEGER;
			case ENUM_BEFESTIGUNG_ART_SIGNALBRUECKE_VALUE: return ENUM_BEFESTIGUNG_ART_SIGNALBRUECKE;
			case ENUM_BEFESTIGUNG_ART_SONSTIGE_VALUE: return ENUM_BEFESTIGUNG_ART_SONSTIGE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ENUMBefestigungArt(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ENUMBefestigungArt
