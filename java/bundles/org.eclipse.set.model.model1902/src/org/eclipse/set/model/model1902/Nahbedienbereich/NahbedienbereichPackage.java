/**
 * Copyright (c) 2022 DB Netz AG and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package org.eclipse.set.model.model1902.Nahbedienbereich;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.set.model.model1902.BasisTypen.BasisTypenPackage;

import org.eclipse.set.model.model1902.Basisobjekte.BasisobjektePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * Dieses Werk ist lizenziert unter der Open Source Lizenz RailPL V1.0.
 * 
 * Weitere Informationen zur Lizenz finden Sie auf
 * http://www.dbnetze.com/planpro
 * 
 * Inhalt der Datei:
 * XML Schema für PlanPro Schnittstelle.
 * 
 * Für Fragen zum Schema wenden Sie sich bitte an Herrn :
 * 
 * Reiner Brödel (reiner.broedel@deutschebahn.com, +49 30 297-57123)
 * 
 * --------------------------------------------------------------------------------
 * 
 * This Document is licensed under the open source license RailPL V1.0.
 * 
 * More information about the license can be found on
 * http://www.dbnetze.com/planpro
 * 
 * Contents of the file:
 * XML Schema for PlanPro interface.
 * Dieses Werk ist lizenziert unter der Open Source Lizenz RailPL V1.0.
 * 
 * Weitere Informationen zur Lizenz finden Sie auf
 * http://www.dbnetze.com/planpro
 * 
 * Inhalt der Datei:
 * XML Schema für PlanPro Schnittstelle.
 * 
 * Für Fragen zum Schema wenden Sie sich bitte an Herrn :
 * 
 * Reiner Brödel (reiner.broedel@deutschebahn.com, +49 30 297-57123)
 * 
 * --------------------------------------------------------------------------------
 * 
 * This Document is licensed under the open source license RailPL V1.0.
 * 
 * More information about the license can be found on
 * http://www.dbnetze.com/planpro
 * 
 * Contents of the file:
 * XML Schema for PlanPro interface.
 * Dieses Werk ist lizenziert unter der Open Source Lizenz RailPL V1.0.
 * 
 * Weitere Informationen zur Lizenz finden Sie auf
 * http://www.dbnetze.com/planpro
 * 
 * Inhalt der Datei:
 * XML Schema für PlanPro Schnittstelle.
 * 
 * Für Fragen zum Schema wenden Sie sich bitte an Herrn :
 * 
 * Reiner Brödel (reiner.broedel@deutschebahn.com, +49 30 297-57123)
 * 
 * --------------------------------------------------------------------------------
 * 
 * This Document is licensed under the open source license RailPL V1.0.
 * 
 * More information about the license can be found on
 * http://www.dbnetze.com/planpro
 * 
 * Contents of the file:
 * XML Schema for PlanPro interface.
 * Dieses Werk ist lizenziert unter der Open Source Lizenz RailPL V1.0.
 * 
 * Weitere Informationen zur Lizenz finden Sie auf
 * http://www.dbnetze.com/planpro
 * 
 * Inhalt der Datei:
 * XML Schema für PlanPro Schnittstelle.
 * 
 * Für Fragen zum Schema wenden Sie sich bitte an Herrn :
 * 
 * Reiner Brödel (reiner.broedel@deutschebahn.com, +49 30 297-57123)
 * 
 * --------------------------------------------------------------------------------
 * 
 * This Document is licensed under the open source license RailPL V1.0.
 * 
 * More information about the license can be found on
 * http://www.dbnetze.com/planpro
 * 
 * Contents of the file:
 * XML Schema for PlanPro interface.
 * <!-- end-model-doc -->
 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NahbedienbereichFactory
 * @model kind="package"
 * @generated
 */
public interface NahbedienbereichPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Nahbedienbereich";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.plan-pro.org/modell/Nahbedienbereich/1.9.0.2";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "nsNahbedienbereich";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	NahbedienbereichPackage eINSTANCE = org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.AWU_TypeClassImpl <em>AWU Type Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.AWU_TypeClassImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getAWU_TypeClass()
	 * @generated
	 */
	int AWU_TYPE_CLASS = 0;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AWU_TYPE_CLASS__ID_BEARBEITUNGSVERMERK = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Wert</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AWU_TYPE_CLASS__WERT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>AWU Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AWU_TYPE_CLASS_FEATURE_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>AWU Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AWU_TYPE_CLASS_OPERATION_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.F_ST_Z_TypeClassImpl <em>FST ZType Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.F_ST_Z_TypeClassImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getF_ST_Z_TypeClass()
	 * @generated
	 */
	int FST_ZTYPE_CLASS = 1;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FST_ZTYPE_CLASS__ID_BEARBEITUNGSVERMERK = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Wert</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FST_ZTYPE_CLASS__WERT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FST ZType Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FST_ZTYPE_CLASS_FEATURE_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>FST ZType Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FST_ZTYPE_CLASS_OPERATION_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.FA_FAE_TypeClassImpl <em>FA FAE Type Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.FA_FAE_TypeClassImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getFA_FAE_TypeClass()
	 * @generated
	 */
	int FA_FAE_TYPE_CLASS = 2;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FA_FAE_TYPE_CLASS__ID_BEARBEITUNGSVERMERK = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Wert</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FA_FAE_TYPE_CLASS__WERT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FA FAE Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FA_FAE_TYPE_CLASS_FEATURE_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>FA FAE Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FA_FAE_TYPE_CLASS_OPERATION_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.Freie_Stellbarkeit_TypeClassImpl <em>Freie Stellbarkeit Type Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.Freie_Stellbarkeit_TypeClassImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getFreie_Stellbarkeit_TypeClass()
	 * @generated
	 */
	int FREIE_STELLBARKEIT_TYPE_CLASS = 3;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREIE_STELLBARKEIT_TYPE_CLASS__ID_BEARBEITUNGSVERMERK = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Wert</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREIE_STELLBARKEIT_TYPE_CLASS__WERT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Freie Stellbarkeit Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREIE_STELLBARKEIT_TYPE_CLASS_FEATURE_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Freie Stellbarkeit Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREIE_STELLBARKEIT_TYPE_CLASS_OPERATION_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NBImpl <em>NB</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NBImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB()
	 * @generated
	 */
	int NB = 4;

	/**
	 * The feature id for the '<em><b>Identitaet</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB__IDENTITAET = BasisobjektePackage.BASIS_OBJEKT__IDENTITAET;

	/**
	 * The feature id for the '<em><b>Basis Objekt Allg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB__BASIS_OBJEKT_ALLG = BasisobjektePackage.BASIS_OBJEKT__BASIS_OBJEKT_ALLG;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB__ID_BEARBEITUNGSVERMERK = BasisobjektePackage.BASIS_OBJEKT__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Objektreferenzen</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB__OBJEKTREFERENZEN = BasisobjektePackage.BASIS_OBJEKT__OBJEKTREFERENZEN;

	/**
	 * The feature id for the '<em><b>Bezeichnung</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB__BEZEICHNUNG = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>NB Allg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB__NB_ALLG = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>NB Funktionalitaet NBR</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB__NB_FUNKTIONALITAET_NBR = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>NB</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_FEATURE_COUNT = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>NB</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_OPERATION_COUNT = BasisobjektePackage.BASIS_OBJEKT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Allg_AttributeGroupImpl <em>NB Allg Attribute Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Allg_AttributeGroupImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Allg_AttributeGroup()
	 * @generated
	 */
	int NB_ALLG_ATTRIBUTE_GROUP = 5;

	/**
	 * The feature id for the '<em><b>NB Art</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ALLG_ATTRIBUTE_GROUP__NB_ART = 0;

	/**
	 * The feature id for the '<em><b>NB Bezeichnung</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ALLG_ATTRIBUTE_GROUP__NB_BEZEICHNUNG = 1;

	/**
	 * The number of structural features of the '<em>NB Allg Attribute Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ALLG_ATTRIBUTE_GROUP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>NB Allg Attribute Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ALLG_ATTRIBUTE_GROUP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Art_TypeClassImpl <em>NB Art Type Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Art_TypeClassImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Art_TypeClass()
	 * @generated
	 */
	int NB_ART_TYPE_CLASS = 6;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ART_TYPE_CLASS__ID_BEARBEITUNGSVERMERK = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Wert</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ART_TYPE_CLASS__WERT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>NB Art Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ART_TYPE_CLASS_FEATURE_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>NB Art Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ART_TYPE_CLASS_OPERATION_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Bedien_Anzeige_ElementImpl <em>NB Bedien Anzeige Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Bedien_Anzeige_ElementImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Bedien_Anzeige_Element()
	 * @generated
	 */
	int NB_BEDIEN_ANZEIGE_ELEMENT = 7;

	/**
	 * The feature id for the '<em><b>Identitaet</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_BEDIEN_ANZEIGE_ELEMENT__IDENTITAET = BasisobjektePackage.BASIS_OBJEKT__IDENTITAET;

	/**
	 * The feature id for the '<em><b>Basis Objekt Allg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_BEDIEN_ANZEIGE_ELEMENT__BASIS_OBJEKT_ALLG = BasisobjektePackage.BASIS_OBJEKT__BASIS_OBJEKT_ALLG;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_BEDIEN_ANZEIGE_ELEMENT__ID_BEARBEITUNGSVERMERK = BasisobjektePackage.BASIS_OBJEKT__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Objektreferenzen</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_BEDIEN_ANZEIGE_ELEMENT__OBJEKTREFERENZEN = BasisobjektePackage.BASIS_OBJEKT__OBJEKTREFERENZEN;

	/**
	 * The feature id for the '<em><b>ID Bedien Anzeige Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_BEDIEN_ANZEIGE_ELEMENT__ID_BEDIEN_ANZEIGE_ELEMENT = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>IDNB Zone</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_BEDIEN_ANZEIGE_ELEMENT__IDNB_ZONE = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>NB Bedien Anzeige Funktionen</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_BEDIEN_ANZEIGE_ELEMENT__NB_BEDIEN_ANZEIGE_FUNKTIONEN = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>NB Bedien Anzeige Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_BEDIEN_ANZEIGE_ELEMENT_FEATURE_COUNT = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>NB Bedien Anzeige Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_BEDIEN_ANZEIGE_ELEMENT_OPERATION_COUNT = BasisobjektePackage.BASIS_OBJEKT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Bedien_Anzeige_Funktionen_AttributeGroupImpl <em>NB Bedien Anzeige Funktionen Attribute Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Bedien_Anzeige_Funktionen_AttributeGroupImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Bedien_Anzeige_Funktionen_AttributeGroup()
	 * @generated
	 */
	int NB_BEDIEN_ANZEIGE_FUNKTIONEN_ATTRIBUTE_GROUP = 8;

	/**
	 * The feature id for the '<em><b>Taste ANF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_BEDIEN_ANZEIGE_FUNKTIONEN_ATTRIBUTE_GROUP__TASTE_ANF = 0;

	/**
	 * The feature id for the '<em><b>Taste FGT</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_BEDIEN_ANZEIGE_FUNKTIONEN_ATTRIBUTE_GROUP__TASTE_FGT = 1;

	/**
	 * The feature id for the '<em><b>Taste WGT</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_BEDIEN_ANZEIGE_FUNKTIONEN_ATTRIBUTE_GROUP__TASTE_WGT = 2;

	/**
	 * The number of structural features of the '<em>NB Bedien Anzeige Funktionen Attribute Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_BEDIEN_ANZEIGE_FUNKTIONEN_ATTRIBUTE_GROUP_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>NB Bedien Anzeige Funktionen Attribute Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_BEDIEN_ANZEIGE_FUNKTIONEN_ATTRIBUTE_GROUP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Bezeichnung_TypeClassImpl <em>NB Bezeichnung Type Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Bezeichnung_TypeClassImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Bezeichnung_TypeClass()
	 * @generated
	 */
	int NB_BEZEICHNUNG_TYPE_CLASS = 9;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_BEZEICHNUNG_TYPE_CLASS__ID_BEARBEITUNGSVERMERK = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Wert</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_BEZEICHNUNG_TYPE_CLASS__WERT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>NB Bezeichnung Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_BEZEICHNUNG_TYPE_CLASS_FEATURE_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>NB Bezeichnung Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_BEZEICHNUNG_TYPE_CLASS_OPERATION_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Funktionalitaet_NB_R_AttributeGroupImpl <em>NB Funktionalitaet NB RAttribute Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Funktionalitaet_NB_R_AttributeGroupImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Funktionalitaet_NB_R_AttributeGroup()
	 * @generated
	 */
	int NB_FUNKTIONALITAET_NB_RATTRIBUTE_GROUP = 10;

	/**
	 * The feature id for the '<em><b>AWU</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_FUNKTIONALITAET_NB_RATTRIBUTE_GROUP__AWU = 0;

	/**
	 * The feature id for the '<em><b>FSTZ</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_FUNKTIONALITAET_NB_RATTRIBUTE_GROUP__FSTZ = 1;

	/**
	 * The feature id for the '<em><b>FAFAE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_FUNKTIONALITAET_NB_RATTRIBUTE_GROUP__FAFAE = 2;

	/**
	 * The feature id for the '<em><b>SBUE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_FUNKTIONALITAET_NB_RATTRIBUTE_GROUP__SBUE = 3;

	/**
	 * The feature id for the '<em><b>SLESLS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_FUNKTIONALITAET_NB_RATTRIBUTE_GROUP__SLESLS = 4;

	/**
	 * The feature id for the '<em><b>WHU</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_FUNKTIONALITAET_NB_RATTRIBUTE_GROUP__WHU = 5;

	/**
	 * The feature id for the '<em><b>WUS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_FUNKTIONALITAET_NB_RATTRIBUTE_GROUP__WUS = 6;

	/**
	 * The number of structural features of the '<em>NB Funktionalitaet NB RAttribute Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_FUNKTIONALITAET_NB_RATTRIBUTE_GROUP_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>NB Funktionalitaet NB RAttribute Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_FUNKTIONALITAET_NB_RATTRIBUTE_GROUP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Grenze_Art_TypeClassImpl <em>NB Grenze Art Type Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Grenze_Art_TypeClassImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Grenze_Art_TypeClass()
	 * @generated
	 */
	int NB_GRENZE_ART_TYPE_CLASS = 11;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_GRENZE_ART_TYPE_CLASS__ID_BEARBEITUNGSVERMERK = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Wert</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_GRENZE_ART_TYPE_CLASS__WERT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>NB Grenze Art Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_GRENZE_ART_TYPE_CLASS_FEATURE_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>NB Grenze Art Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_GRENZE_ART_TYPE_CLASS_OPERATION_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Rueckgabevoraussetzung_TypeClassImpl <em>NB Rueckgabevoraussetzung Type Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Rueckgabevoraussetzung_TypeClassImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Rueckgabevoraussetzung_TypeClass()
	 * @generated
	 */
	int NB_RUECKGABEVORAUSSETZUNG_TYPE_CLASS = 12;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_RUECKGABEVORAUSSETZUNG_TYPE_CLASS__ID_BEARBEITUNGSVERMERK = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Wert</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_RUECKGABEVORAUSSETZUNG_TYPE_CLASS__WERT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>NB Rueckgabevoraussetzung Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_RUECKGABEVORAUSSETZUNG_TYPE_CLASS_FEATURE_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>NB Rueckgabevoraussetzung Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_RUECKGABEVORAUSSETZUNG_TYPE_CLASS_OPERATION_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Verhaeltnis_Besonders_TypeClassImpl <em>NB Verhaeltnis Besonders Type Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Verhaeltnis_Besonders_TypeClassImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Verhaeltnis_Besonders_TypeClass()
	 * @generated
	 */
	int NB_VERHAELTNIS_BESONDERS_TYPE_CLASS = 13;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_VERHAELTNIS_BESONDERS_TYPE_CLASS__ID_BEARBEITUNGSVERMERK = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Wert</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_VERHAELTNIS_BESONDERS_TYPE_CLASS__WERT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>NB Verhaeltnis Besonders Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_VERHAELTNIS_BESONDERS_TYPE_CLASS_FEATURE_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>NB Verhaeltnis Besonders Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_VERHAELTNIS_BESONDERS_TYPE_CLASS_OPERATION_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_ZoneImpl <em>NB Zone</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_ZoneImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone()
	 * @generated
	 */
	int NB_ZONE = 14;

	/**
	 * The feature id for the '<em><b>Identitaet</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE__IDENTITAET = BasisobjektePackage.BASIS_OBJEKT__IDENTITAET;

	/**
	 * The feature id for the '<em><b>Basis Objekt Allg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE__BASIS_OBJEKT_ALLG = BasisobjektePackage.BASIS_OBJEKT__BASIS_OBJEKT_ALLG;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE__ID_BEARBEITUNGSVERMERK = BasisobjektePackage.BASIS_OBJEKT__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Objektreferenzen</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE__OBJEKTREFERENZEN = BasisobjektePackage.BASIS_OBJEKT__OBJEKTREFERENZEN;

	/**
	 * The feature id for the '<em><b>IDNB</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE__IDNB = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>IDNB Zone</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE__IDNB_ZONE = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>NB Zone Allg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE__NB_ZONE_ALLG = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>NB Zone</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_FEATURE_COUNT = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>NB Zone</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_OPERATION_COUNT = BasisobjektePackage.BASIS_OBJEKT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Allg_AttributeGroupImpl <em>NB Zone Allg Attribute Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Allg_AttributeGroupImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone_Allg_AttributeGroup()
	 * @generated
	 */
	int NB_ZONE_ALLG_ATTRIBUTE_GROUP = 15;

	/**
	 * The feature id for the '<em><b>NB Verhaeltnis Besonders</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ALLG_ATTRIBUTE_GROUP__NB_VERHAELTNIS_BESONDERS = 0;

	/**
	 * The feature id for the '<em><b>NB Zone Bezeichnung</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ALLG_ATTRIBUTE_GROUP__NB_ZONE_BEZEICHNUNG = 1;

	/**
	 * The feature id for the '<em><b>Rang</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ALLG_ATTRIBUTE_GROUP__RANG = 2;

	/**
	 * The number of structural features of the '<em>NB Zone Allg Attribute Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ALLG_ATTRIBUTE_GROUP_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>NB Zone Allg Attribute Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ALLG_ATTRIBUTE_GROUP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Allg_TypeClassImpl <em>NB Zone Allg Type Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Allg_TypeClassImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone_Allg_TypeClass()
	 * @generated
	 */
	int NB_ZONE_ALLG_TYPE_CLASS = 16;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ALLG_TYPE_CLASS__ID_BEARBEITUNGSVERMERK = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Wert</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ALLG_TYPE_CLASS__WERT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>NB Zone Allg Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ALLG_TYPE_CLASS_FEATURE_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>NB Zone Allg Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ALLG_TYPE_CLASS_OPERATION_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Bezeichnung_TypeClassImpl <em>NB Zone Bezeichnung Type Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Bezeichnung_TypeClassImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone_Bezeichnung_TypeClass()
	 * @generated
	 */
	int NB_ZONE_BEZEICHNUNG_TYPE_CLASS = 17;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_BEZEICHNUNG_TYPE_CLASS__ID_BEARBEITUNGSVERMERK = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Wert</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_BEZEICHNUNG_TYPE_CLASS__WERT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>NB Zone Bezeichnung Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_BEZEICHNUNG_TYPE_CLASS_FEATURE_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>NB Zone Bezeichnung Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_BEZEICHNUNG_TYPE_CLASS_OPERATION_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_ElementImpl <em>NB Zone Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_ElementImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone_Element()
	 * @generated
	 */
	int NB_ZONE_ELEMENT = 18;

	/**
	 * The feature id for the '<em><b>Identitaet</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ELEMENT__IDENTITAET = BasisobjektePackage.BASIS_OBJEKT__IDENTITAET;

	/**
	 * The feature id for the '<em><b>Basis Objekt Allg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ELEMENT__BASIS_OBJEKT_ALLG = BasisobjektePackage.BASIS_OBJEKT__BASIS_OBJEKT_ALLG;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ELEMENT__ID_BEARBEITUNGSVERMERK = BasisobjektePackage.BASIS_OBJEKT__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Objektreferenzen</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ELEMENT__OBJEKTREFERENZEN = BasisobjektePackage.BASIS_OBJEKT__OBJEKTREFERENZEN;

	/**
	 * The feature id for the '<em><b>IDNB Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ELEMENT__IDNB_ELEMENT = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>IDNB Zone</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ELEMENT__IDNB_ZONE = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>NB Zone Element Allg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ELEMENT__NB_ZONE_ELEMENT_ALLG = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>NB Zone Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ELEMENT_FEATURE_COUNT = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>NB Zone Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ELEMENT_OPERATION_COUNT = BasisobjektePackage.BASIS_OBJEKT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Element_Allg_AttributeGroupImpl <em>NB Zone Element Allg Attribute Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Element_Allg_AttributeGroupImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone_Element_Allg_AttributeGroup()
	 * @generated
	 */
	int NB_ZONE_ELEMENT_ALLG_ATTRIBUTE_GROUP = 19;

	/**
	 * The feature id for the '<em><b>Freie Stellbarkeit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ELEMENT_ALLG_ATTRIBUTE_GROUP__FREIE_STELLBARKEIT = 0;

	/**
	 * The feature id for the '<em><b>NB Rueckgabevoraussetzung</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ELEMENT_ALLG_ATTRIBUTE_GROUP__NB_RUECKGABEVORAUSSETZUNG = 1;

	/**
	 * The number of structural features of the '<em>NB Zone Element Allg Attribute Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ELEMENT_ALLG_ATTRIBUTE_GROUP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>NB Zone Element Allg Attribute Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_ELEMENT_ALLG_ATTRIBUTE_GROUP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_GrenzeImpl <em>NB Zone Grenze</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_GrenzeImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone_Grenze()
	 * @generated
	 */
	int NB_ZONE_GRENZE = 20;

	/**
	 * The feature id for the '<em><b>Identitaet</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_GRENZE__IDENTITAET = BasisobjektePackage.BASIS_OBJEKT__IDENTITAET;

	/**
	 * The feature id for the '<em><b>Basis Objekt Allg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_GRENZE__BASIS_OBJEKT_ALLG = BasisobjektePackage.BASIS_OBJEKT__BASIS_OBJEKT_ALLG;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_GRENZE__ID_BEARBEITUNGSVERMERK = BasisobjektePackage.BASIS_OBJEKT__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Objektreferenzen</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_GRENZE__OBJEKTREFERENZEN = BasisobjektePackage.BASIS_OBJEKT__OBJEKTREFERENZEN;

	/**
	 * The feature id for the '<em><b>ID Markanter Punkt</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_GRENZE__ID_MARKANTER_PUNKT = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>IDNB Zone</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_GRENZE__IDNB_ZONE = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>NB Grenze Art</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_GRENZE__NB_GRENZE_ART = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>NB Zone Grenze</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_GRENZE_FEATURE_COUNT = BasisobjektePackage.BASIS_OBJEKT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>NB Zone Grenze</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_GRENZE_OPERATION_COUNT = BasisobjektePackage.BASIS_OBJEKT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Reihenfolgezwang_AttributeGroupImpl <em>NB Zone Reihenfolgezwang Attribute Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Reihenfolgezwang_AttributeGroupImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone_Reihenfolgezwang_AttributeGroup()
	 * @generated
	 */
	int NB_ZONE_REIHENFOLGEZWANG_ATTRIBUTE_GROUP = 21;

	/**
	 * The feature id for the '<em><b>NB Zone Allg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_REIHENFOLGEZWANG_ATTRIBUTE_GROUP__NB_ZONE_ALLG = 0;

	/**
	 * The number of structural features of the '<em>NB Zone Reihenfolgezwang Attribute Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_REIHENFOLGEZWANG_ATTRIBUTE_GROUP_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>NB Zone Reihenfolgezwang Attribute Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_ZONE_REIHENFOLGEZWANG_ATTRIBUTE_GROUP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.Rang_TypeClassImpl <em>Rang Type Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.Rang_TypeClassImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getRang_TypeClass()
	 * @generated
	 */
	int RANG_TYPE_CLASS = 22;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANG_TYPE_CLASS__ID_BEARBEITUNGSVERMERK = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Wert</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANG_TYPE_CLASS__WERT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Rang Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANG_TYPE_CLASS_FEATURE_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Rang Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANG_TYPE_CLASS_OPERATION_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.SBUE_TypeClassImpl <em>SBUE Type Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.SBUE_TypeClassImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getSBUE_TypeClass()
	 * @generated
	 */
	int SBUE_TYPE_CLASS = 23;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBUE_TYPE_CLASS__ID_BEARBEITUNGSVERMERK = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Wert</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBUE_TYPE_CLASS__WERT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SBUE Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBUE_TYPE_CLASS_FEATURE_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>SBUE Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBUE_TYPE_CLASS_OPERATION_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.SLE_SLS_TypeClassImpl <em>SLE SLS Type Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.SLE_SLS_TypeClassImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getSLE_SLS_TypeClass()
	 * @generated
	 */
	int SLE_SLS_TYPE_CLASS = 24;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLE_SLS_TYPE_CLASS__ID_BEARBEITUNGSVERMERK = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Wert</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLE_SLS_TYPE_CLASS__WERT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SLE SLS Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLE_SLS_TYPE_CLASS_FEATURE_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>SLE SLS Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLE_SLS_TYPE_CLASS_OPERATION_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.Taste_ANF_TypeClassImpl <em>Taste ANF Type Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.Taste_ANF_TypeClassImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getTaste_ANF_TypeClass()
	 * @generated
	 */
	int TASTE_ANF_TYPE_CLASS = 25;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASTE_ANF_TYPE_CLASS__ID_BEARBEITUNGSVERMERK = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Wert</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASTE_ANF_TYPE_CLASS__WERT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Taste ANF Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASTE_ANF_TYPE_CLASS_FEATURE_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Taste ANF Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASTE_ANF_TYPE_CLASS_OPERATION_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.Taste_FGT_TypeClassImpl <em>Taste FGT Type Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.Taste_FGT_TypeClassImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getTaste_FGT_TypeClass()
	 * @generated
	 */
	int TASTE_FGT_TYPE_CLASS = 26;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASTE_FGT_TYPE_CLASS__ID_BEARBEITUNGSVERMERK = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Wert</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASTE_FGT_TYPE_CLASS__WERT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Taste FGT Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASTE_FGT_TYPE_CLASS_FEATURE_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Taste FGT Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASTE_FGT_TYPE_CLASS_OPERATION_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.Taste_WGT_TypeClassImpl <em>Taste WGT Type Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.Taste_WGT_TypeClassImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getTaste_WGT_TypeClass()
	 * @generated
	 */
	int TASTE_WGT_TYPE_CLASS = 27;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASTE_WGT_TYPE_CLASS__ID_BEARBEITUNGSVERMERK = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Wert</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASTE_WGT_TYPE_CLASS__WERT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Taste WGT Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASTE_WGT_TYPE_CLASS_FEATURE_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Taste WGT Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASTE_WGT_TYPE_CLASS_OPERATION_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.WHU_TypeClassImpl <em>WHU Type Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.WHU_TypeClassImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getWHU_TypeClass()
	 * @generated
	 */
	int WHU_TYPE_CLASS = 28;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHU_TYPE_CLASS__ID_BEARBEITUNGSVERMERK = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Wert</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHU_TYPE_CLASS__WERT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>WHU Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHU_TYPE_CLASS_FEATURE_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>WHU Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHU_TYPE_CLASS_OPERATION_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.WUS_TypeClassImpl <em>WUS Type Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.WUS_TypeClassImpl
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getWUS_TypeClass()
	 * @generated
	 */
	int WUS_TYPE_CLASS = 29;

	/**
	 * The feature id for the '<em><b>ID Bearbeitungsvermerk</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WUS_TYPE_CLASS__ID_BEARBEITUNGSVERMERK = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP__ID_BEARBEITUNGSVERMERK;

	/**
	 * The feature id for the '<em><b>Wert</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WUS_TYPE_CLASS__WERT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>WUS Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WUS_TYPE_CLASS_FEATURE_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>WUS Type Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WUS_TYPE_CLASS_OPERATION_COUNT = BasisTypenPackage.BASIS_ATTRIBUT_ATTRIBUTE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBArt <em>ENUMNB Art</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBArt
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getENUMNBArt()
	 * @generated
	 */
	int ENUMNB_ART = 30;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBGrenzeArt <em>ENUMNB Grenze Art</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBGrenzeArt
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getENUMNBGrenzeArt()
	 * @generated
	 */
	int ENUMNB_GRENZE_ART = 31;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBRueckgabevoraussetzung <em>ENUMNB Rueckgabevoraussetzung</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBRueckgabevoraussetzung
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getENUMNBRueckgabevoraussetzung()
	 * @generated
	 */
	int ENUMNB_RUECKGABEVORAUSSETZUNG = 32;

	/**
	 * The meta object id for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBVerhaeltnisBesonders <em>ENUMNB Verhaeltnis Besonders</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBVerhaeltnisBesonders
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getENUMNBVerhaeltnisBesonders()
	 * @generated
	 */
	int ENUMNB_VERHAELTNIS_BESONDERS = 33;

	/**
	 * The meta object id for the '<em>ENUMNB Art Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBArt
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getENUMNBArtObject()
	 * @generated
	 */
	int ENUMNB_ART_OBJECT = 34;

	/**
	 * The meta object id for the '<em>ENUMNB Grenze Art Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBGrenzeArt
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getENUMNBGrenzeArtObject()
	 * @generated
	 */
	int ENUMNB_GRENZE_ART_OBJECT = 35;

	/**
	 * The meta object id for the '<em>ENUMNB Rueckgabevoraussetzung Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBRueckgabevoraussetzung
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getENUMNBRueckgabevoraussetzungObject()
	 * @generated
	 */
	int ENUMNB_RUECKGABEVORAUSSETZUNG_OBJECT = 36;

	/**
	 * The meta object id for the '<em>ENUMNB Verhaeltnis Besonders Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBVerhaeltnisBesonders
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getENUMNBVerhaeltnisBesondersObject()
	 * @generated
	 */
	int ENUMNB_VERHAELTNIS_BESONDERS_OBJECT = 37;

	/**
	 * The meta object id for the '<em>NB Bezeichnung Type</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.math.BigInteger
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Bezeichnung_Type()
	 * @generated
	 */
	int NB_BEZEICHNUNG_TYPE = 38;

	/**
	 * The meta object id for the '<em>NB Zone Allg Type</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.math.BigInteger
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone_Allg_Type()
	 * @generated
	 */
	int NB_ZONE_ALLG_TYPE = 39;

	/**
	 * The meta object id for the '<em>NB Zone Bezeichnung Type</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.math.BigInteger
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone_Bezeichnung_Type()
	 * @generated
	 */
	int NB_ZONE_BEZEICHNUNG_TYPE = 40;

	/**
	 * The meta object id for the '<em>Rang Type</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.math.BigInteger
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getRang_Type()
	 * @generated
	 */
	int RANG_TYPE = 41;


	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.AWU_TypeClass <em>AWU Type Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AWU Type Class</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.AWU_TypeClass
	 * @generated
	 */
	EClass getAWU_TypeClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.set.model.model1902.Nahbedienbereich.AWU_TypeClass#getWert <em>Wert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wert</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.AWU_TypeClass#getWert()
	 * @see #getAWU_TypeClass()
	 * @generated
	 */
	EAttribute getAWU_TypeClass_Wert();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.F_ST_Z_TypeClass <em>FST ZType Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FST ZType Class</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.F_ST_Z_TypeClass
	 * @generated
	 */
	EClass getF_ST_Z_TypeClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.set.model.model1902.Nahbedienbereich.F_ST_Z_TypeClass#getWert <em>Wert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wert</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.F_ST_Z_TypeClass#getWert()
	 * @see #getF_ST_Z_TypeClass()
	 * @generated
	 */
	EAttribute getF_ST_Z_TypeClass_Wert();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.FA_FAE_TypeClass <em>FA FAE Type Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FA FAE Type Class</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.FA_FAE_TypeClass
	 * @generated
	 */
	EClass getFA_FAE_TypeClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.set.model.model1902.Nahbedienbereich.FA_FAE_TypeClass#getWert <em>Wert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wert</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.FA_FAE_TypeClass#getWert()
	 * @see #getFA_FAE_TypeClass()
	 * @generated
	 */
	EAttribute getFA_FAE_TypeClass_Wert();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.Freie_Stellbarkeit_TypeClass <em>Freie Stellbarkeit Type Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Freie Stellbarkeit Type Class</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.Freie_Stellbarkeit_TypeClass
	 * @generated
	 */
	EClass getFreie_Stellbarkeit_TypeClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.set.model.model1902.Nahbedienbereich.Freie_Stellbarkeit_TypeClass#getWert <em>Wert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wert</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.Freie_Stellbarkeit_TypeClass#getWert()
	 * @see #getFreie_Stellbarkeit_TypeClass()
	 * @generated
	 */
	EAttribute getFreie_Stellbarkeit_TypeClass_Wert();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB <em>NB</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NB</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB
	 * @generated
	 */
	EClass getNB();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB#getBezeichnung <em>Bezeichnung</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Bezeichnung</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB#getBezeichnung()
	 * @see #getNB()
	 * @generated
	 */
	EReference getNB_Bezeichnung();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB#getNBAllg <em>NB Allg</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>NB Allg</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB#getNBAllg()
	 * @see #getNB()
	 * @generated
	 */
	EReference getNB_NBAllg();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB#getNBFunktionalitaetNBR <em>NB Funktionalitaet NBR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>NB Funktionalitaet NBR</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB#getNBFunktionalitaetNBR()
	 * @see #getNB()
	 * @generated
	 */
	EReference getNB_NBFunktionalitaetNBR();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Allg_AttributeGroup <em>NB Allg Attribute Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NB Allg Attribute Group</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Allg_AttributeGroup
	 * @generated
	 */
	EClass getNB_Allg_AttributeGroup();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Allg_AttributeGroup#getNBArt <em>NB Art</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>NB Art</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Allg_AttributeGroup#getNBArt()
	 * @see #getNB_Allg_AttributeGroup()
	 * @generated
	 */
	EReference getNB_Allg_AttributeGroup_NBArt();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Allg_AttributeGroup#getNBBezeichnung <em>NB Bezeichnung</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>NB Bezeichnung</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Allg_AttributeGroup#getNBBezeichnung()
	 * @see #getNB_Allg_AttributeGroup()
	 * @generated
	 */
	EReference getNB_Allg_AttributeGroup_NBBezeichnung();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Art_TypeClass <em>NB Art Type Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NB Art Type Class</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Art_TypeClass
	 * @generated
	 */
	EClass getNB_Art_TypeClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Art_TypeClass#getWert <em>Wert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wert</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Art_TypeClass#getWert()
	 * @see #getNB_Art_TypeClass()
	 * @generated
	 */
	EAttribute getNB_Art_TypeClass_Wert();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bedien_Anzeige_Element <em>NB Bedien Anzeige Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NB Bedien Anzeige Element</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bedien_Anzeige_Element
	 * @generated
	 */
	EClass getNB_Bedien_Anzeige_Element();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bedien_Anzeige_Element#getIDBedienAnzeigeElement <em>ID Bedien Anzeige Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>ID Bedien Anzeige Element</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bedien_Anzeige_Element#getIDBedienAnzeigeElement()
	 * @see #getNB_Bedien_Anzeige_Element()
	 * @generated
	 */
	EReference getNB_Bedien_Anzeige_Element_IDBedienAnzeigeElement();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bedien_Anzeige_Element#getIDNBZone <em>IDNB Zone</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>IDNB Zone</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bedien_Anzeige_Element#getIDNBZone()
	 * @see #getNB_Bedien_Anzeige_Element()
	 * @generated
	 */
	EReference getNB_Bedien_Anzeige_Element_IDNBZone();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bedien_Anzeige_Element#getNBBedienAnzeigeFunktionen <em>NB Bedien Anzeige Funktionen</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>NB Bedien Anzeige Funktionen</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bedien_Anzeige_Element#getNBBedienAnzeigeFunktionen()
	 * @see #getNB_Bedien_Anzeige_Element()
	 * @generated
	 */
	EReference getNB_Bedien_Anzeige_Element_NBBedienAnzeigeFunktionen();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bedien_Anzeige_Funktionen_AttributeGroup <em>NB Bedien Anzeige Funktionen Attribute Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NB Bedien Anzeige Funktionen Attribute Group</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bedien_Anzeige_Funktionen_AttributeGroup
	 * @generated
	 */
	EClass getNB_Bedien_Anzeige_Funktionen_AttributeGroup();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bedien_Anzeige_Funktionen_AttributeGroup#getTasteANF <em>Taste ANF</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Taste ANF</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bedien_Anzeige_Funktionen_AttributeGroup#getTasteANF()
	 * @see #getNB_Bedien_Anzeige_Funktionen_AttributeGroup()
	 * @generated
	 */
	EReference getNB_Bedien_Anzeige_Funktionen_AttributeGroup_TasteANF();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bedien_Anzeige_Funktionen_AttributeGroup#getTasteFGT <em>Taste FGT</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Taste FGT</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bedien_Anzeige_Funktionen_AttributeGroup#getTasteFGT()
	 * @see #getNB_Bedien_Anzeige_Funktionen_AttributeGroup()
	 * @generated
	 */
	EReference getNB_Bedien_Anzeige_Funktionen_AttributeGroup_TasteFGT();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bedien_Anzeige_Funktionen_AttributeGroup#getTasteWGT <em>Taste WGT</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Taste WGT</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bedien_Anzeige_Funktionen_AttributeGroup#getTasteWGT()
	 * @see #getNB_Bedien_Anzeige_Funktionen_AttributeGroup()
	 * @generated
	 */
	EReference getNB_Bedien_Anzeige_Funktionen_AttributeGroup_TasteWGT();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bezeichnung_TypeClass <em>NB Bezeichnung Type Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NB Bezeichnung Type Class</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bezeichnung_TypeClass
	 * @generated
	 */
	EClass getNB_Bezeichnung_TypeClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bezeichnung_TypeClass#getWert <em>Wert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wert</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Bezeichnung_TypeClass#getWert()
	 * @see #getNB_Bezeichnung_TypeClass()
	 * @generated
	 */
	EAttribute getNB_Bezeichnung_TypeClass_Wert();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Funktionalitaet_NB_R_AttributeGroup <em>NB Funktionalitaet NB RAttribute Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NB Funktionalitaet NB RAttribute Group</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Funktionalitaet_NB_R_AttributeGroup
	 * @generated
	 */
	EClass getNB_Funktionalitaet_NB_R_AttributeGroup();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Funktionalitaet_NB_R_AttributeGroup#getAWU <em>AWU</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>AWU</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Funktionalitaet_NB_R_AttributeGroup#getAWU()
	 * @see #getNB_Funktionalitaet_NB_R_AttributeGroup()
	 * @generated
	 */
	EReference getNB_Funktionalitaet_NB_R_AttributeGroup_AWU();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Funktionalitaet_NB_R_AttributeGroup#getFSTZ <em>FSTZ</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>FSTZ</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Funktionalitaet_NB_R_AttributeGroup#getFSTZ()
	 * @see #getNB_Funktionalitaet_NB_R_AttributeGroup()
	 * @generated
	 */
	EReference getNB_Funktionalitaet_NB_R_AttributeGroup_FSTZ();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Funktionalitaet_NB_R_AttributeGroup#getFAFAE <em>FAFAE</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>FAFAE</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Funktionalitaet_NB_R_AttributeGroup#getFAFAE()
	 * @see #getNB_Funktionalitaet_NB_R_AttributeGroup()
	 * @generated
	 */
	EReference getNB_Funktionalitaet_NB_R_AttributeGroup_FAFAE();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Funktionalitaet_NB_R_AttributeGroup#getSBUE <em>SBUE</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>SBUE</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Funktionalitaet_NB_R_AttributeGroup#getSBUE()
	 * @see #getNB_Funktionalitaet_NB_R_AttributeGroup()
	 * @generated
	 */
	EReference getNB_Funktionalitaet_NB_R_AttributeGroup_SBUE();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Funktionalitaet_NB_R_AttributeGroup#getSLESLS <em>SLESLS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>SLESLS</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Funktionalitaet_NB_R_AttributeGroup#getSLESLS()
	 * @see #getNB_Funktionalitaet_NB_R_AttributeGroup()
	 * @generated
	 */
	EReference getNB_Funktionalitaet_NB_R_AttributeGroup_SLESLS();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Funktionalitaet_NB_R_AttributeGroup#getWHU <em>WHU</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>WHU</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Funktionalitaet_NB_R_AttributeGroup#getWHU()
	 * @see #getNB_Funktionalitaet_NB_R_AttributeGroup()
	 * @generated
	 */
	EReference getNB_Funktionalitaet_NB_R_AttributeGroup_WHU();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Funktionalitaet_NB_R_AttributeGroup#getWUS <em>WUS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>WUS</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Funktionalitaet_NB_R_AttributeGroup#getWUS()
	 * @see #getNB_Funktionalitaet_NB_R_AttributeGroup()
	 * @generated
	 */
	EReference getNB_Funktionalitaet_NB_R_AttributeGroup_WUS();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Grenze_Art_TypeClass <em>NB Grenze Art Type Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NB Grenze Art Type Class</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Grenze_Art_TypeClass
	 * @generated
	 */
	EClass getNB_Grenze_Art_TypeClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Grenze_Art_TypeClass#getWert <em>Wert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wert</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Grenze_Art_TypeClass#getWert()
	 * @see #getNB_Grenze_Art_TypeClass()
	 * @generated
	 */
	EAttribute getNB_Grenze_Art_TypeClass_Wert();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Rueckgabevoraussetzung_TypeClass <em>NB Rueckgabevoraussetzung Type Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NB Rueckgabevoraussetzung Type Class</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Rueckgabevoraussetzung_TypeClass
	 * @generated
	 */
	EClass getNB_Rueckgabevoraussetzung_TypeClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Rueckgabevoraussetzung_TypeClass#getWert <em>Wert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wert</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Rueckgabevoraussetzung_TypeClass#getWert()
	 * @see #getNB_Rueckgabevoraussetzung_TypeClass()
	 * @generated
	 */
	EAttribute getNB_Rueckgabevoraussetzung_TypeClass_Wert();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Verhaeltnis_Besonders_TypeClass <em>NB Verhaeltnis Besonders Type Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NB Verhaeltnis Besonders Type Class</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Verhaeltnis_Besonders_TypeClass
	 * @generated
	 */
	EClass getNB_Verhaeltnis_Besonders_TypeClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Verhaeltnis_Besonders_TypeClass#getWert <em>Wert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wert</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Verhaeltnis_Besonders_TypeClass#getWert()
	 * @see #getNB_Verhaeltnis_Besonders_TypeClass()
	 * @generated
	 */
	EAttribute getNB_Verhaeltnis_Besonders_TypeClass_Wert();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone <em>NB Zone</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NB Zone</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone
	 * @generated
	 */
	EClass getNB_Zone();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone#getIDNB <em>IDNB</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>IDNB</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone#getIDNB()
	 * @see #getNB_Zone()
	 * @generated
	 */
	EReference getNB_Zone_IDNB();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone#getIDNBZone <em>IDNB Zone</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>IDNB Zone</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone#getIDNBZone()
	 * @see #getNB_Zone()
	 * @generated
	 */
	EReference getNB_Zone_IDNBZone();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone#getNBZoneAllg <em>NB Zone Allg</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>NB Zone Allg</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone#getNBZoneAllg()
	 * @see #getNB_Zone()
	 * @generated
	 */
	EReference getNB_Zone_NBZoneAllg();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Allg_AttributeGroup <em>NB Zone Allg Attribute Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NB Zone Allg Attribute Group</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Allg_AttributeGroup
	 * @generated
	 */
	EClass getNB_Zone_Allg_AttributeGroup();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Allg_AttributeGroup#getNBVerhaeltnisBesonders <em>NB Verhaeltnis Besonders</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>NB Verhaeltnis Besonders</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Allg_AttributeGroup#getNBVerhaeltnisBesonders()
	 * @see #getNB_Zone_Allg_AttributeGroup()
	 * @generated
	 */
	EReference getNB_Zone_Allg_AttributeGroup_NBVerhaeltnisBesonders();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Allg_AttributeGroup#getNBZoneBezeichnung <em>NB Zone Bezeichnung</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>NB Zone Bezeichnung</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Allg_AttributeGroup#getNBZoneBezeichnung()
	 * @see #getNB_Zone_Allg_AttributeGroup()
	 * @generated
	 */
	EReference getNB_Zone_Allg_AttributeGroup_NBZoneBezeichnung();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Allg_AttributeGroup#getRang <em>Rang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Rang</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Allg_AttributeGroup#getRang()
	 * @see #getNB_Zone_Allg_AttributeGroup()
	 * @generated
	 */
	EReference getNB_Zone_Allg_AttributeGroup_Rang();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Allg_TypeClass <em>NB Zone Allg Type Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NB Zone Allg Type Class</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Allg_TypeClass
	 * @generated
	 */
	EClass getNB_Zone_Allg_TypeClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Allg_TypeClass#getWert <em>Wert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wert</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Allg_TypeClass#getWert()
	 * @see #getNB_Zone_Allg_TypeClass()
	 * @generated
	 */
	EAttribute getNB_Zone_Allg_TypeClass_Wert();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Bezeichnung_TypeClass <em>NB Zone Bezeichnung Type Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NB Zone Bezeichnung Type Class</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Bezeichnung_TypeClass
	 * @generated
	 */
	EClass getNB_Zone_Bezeichnung_TypeClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Bezeichnung_TypeClass#getWert <em>Wert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wert</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Bezeichnung_TypeClass#getWert()
	 * @see #getNB_Zone_Bezeichnung_TypeClass()
	 * @generated
	 */
	EAttribute getNB_Zone_Bezeichnung_TypeClass_Wert();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Element <em>NB Zone Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NB Zone Element</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Element
	 * @generated
	 */
	EClass getNB_Zone_Element();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Element#getIDNBElement <em>IDNB Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>IDNB Element</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Element#getIDNBElement()
	 * @see #getNB_Zone_Element()
	 * @generated
	 */
	EReference getNB_Zone_Element_IDNBElement();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Element#getIDNBZone <em>IDNB Zone</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>IDNB Zone</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Element#getIDNBZone()
	 * @see #getNB_Zone_Element()
	 * @generated
	 */
	EReference getNB_Zone_Element_IDNBZone();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Element#getNBZoneElementAllg <em>NB Zone Element Allg</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>NB Zone Element Allg</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Element#getNBZoneElementAllg()
	 * @see #getNB_Zone_Element()
	 * @generated
	 */
	EReference getNB_Zone_Element_NBZoneElementAllg();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Element_Allg_AttributeGroup <em>NB Zone Element Allg Attribute Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NB Zone Element Allg Attribute Group</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Element_Allg_AttributeGroup
	 * @generated
	 */
	EClass getNB_Zone_Element_Allg_AttributeGroup();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Element_Allg_AttributeGroup#getFreieStellbarkeit <em>Freie Stellbarkeit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Freie Stellbarkeit</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Element_Allg_AttributeGroup#getFreieStellbarkeit()
	 * @see #getNB_Zone_Element_Allg_AttributeGroup()
	 * @generated
	 */
	EReference getNB_Zone_Element_Allg_AttributeGroup_FreieStellbarkeit();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Element_Allg_AttributeGroup#getNBRueckgabevoraussetzung <em>NB Rueckgabevoraussetzung</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>NB Rueckgabevoraussetzung</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Element_Allg_AttributeGroup#getNBRueckgabevoraussetzung()
	 * @see #getNB_Zone_Element_Allg_AttributeGroup()
	 * @generated
	 */
	EReference getNB_Zone_Element_Allg_AttributeGroup_NBRueckgabevoraussetzung();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Grenze <em>NB Zone Grenze</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NB Zone Grenze</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Grenze
	 * @generated
	 */
	EClass getNB_Zone_Grenze();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Grenze#getIDMarkanterPunkt <em>ID Markanter Punkt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>ID Markanter Punkt</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Grenze#getIDMarkanterPunkt()
	 * @see #getNB_Zone_Grenze()
	 * @generated
	 */
	EReference getNB_Zone_Grenze_IDMarkanterPunkt();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Grenze#getIDNBZone <em>IDNB Zone</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>IDNB Zone</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Grenze#getIDNBZone()
	 * @see #getNB_Zone_Grenze()
	 * @generated
	 */
	EReference getNB_Zone_Grenze_IDNBZone();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Grenze#getNBGrenzeArt <em>NB Grenze Art</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>NB Grenze Art</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Grenze#getNBGrenzeArt()
	 * @see #getNB_Zone_Grenze()
	 * @generated
	 */
	EReference getNB_Zone_Grenze_NBGrenzeArt();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Reihenfolgezwang_AttributeGroup <em>NB Zone Reihenfolgezwang Attribute Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NB Zone Reihenfolgezwang Attribute Group</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Reihenfolgezwang_AttributeGroup
	 * @generated
	 */
	EClass getNB_Zone_Reihenfolgezwang_AttributeGroup();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Reihenfolgezwang_AttributeGroup#getNBZoneAllg <em>NB Zone Allg</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>NB Zone Allg</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.NB_Zone_Reihenfolgezwang_AttributeGroup#getNBZoneAllg()
	 * @see #getNB_Zone_Reihenfolgezwang_AttributeGroup()
	 * @generated
	 */
	EReference getNB_Zone_Reihenfolgezwang_AttributeGroup_NBZoneAllg();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.Rang_TypeClass <em>Rang Type Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rang Type Class</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.Rang_TypeClass
	 * @generated
	 */
	EClass getRang_TypeClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.set.model.model1902.Nahbedienbereich.Rang_TypeClass#getWert <em>Wert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wert</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.Rang_TypeClass#getWert()
	 * @see #getRang_TypeClass()
	 * @generated
	 */
	EAttribute getRang_TypeClass_Wert();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.SBUE_TypeClass <em>SBUE Type Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SBUE Type Class</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.SBUE_TypeClass
	 * @generated
	 */
	EClass getSBUE_TypeClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.set.model.model1902.Nahbedienbereich.SBUE_TypeClass#getWert <em>Wert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wert</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.SBUE_TypeClass#getWert()
	 * @see #getSBUE_TypeClass()
	 * @generated
	 */
	EAttribute getSBUE_TypeClass_Wert();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.SLE_SLS_TypeClass <em>SLE SLS Type Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SLE SLS Type Class</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.SLE_SLS_TypeClass
	 * @generated
	 */
	EClass getSLE_SLS_TypeClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.set.model.model1902.Nahbedienbereich.SLE_SLS_TypeClass#getWert <em>Wert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wert</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.SLE_SLS_TypeClass#getWert()
	 * @see #getSLE_SLS_TypeClass()
	 * @generated
	 */
	EAttribute getSLE_SLS_TypeClass_Wert();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.Taste_ANF_TypeClass <em>Taste ANF Type Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Taste ANF Type Class</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.Taste_ANF_TypeClass
	 * @generated
	 */
	EClass getTaste_ANF_TypeClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.set.model.model1902.Nahbedienbereich.Taste_ANF_TypeClass#getWert <em>Wert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wert</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.Taste_ANF_TypeClass#getWert()
	 * @see #getTaste_ANF_TypeClass()
	 * @generated
	 */
	EAttribute getTaste_ANF_TypeClass_Wert();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.Taste_FGT_TypeClass <em>Taste FGT Type Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Taste FGT Type Class</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.Taste_FGT_TypeClass
	 * @generated
	 */
	EClass getTaste_FGT_TypeClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.set.model.model1902.Nahbedienbereich.Taste_FGT_TypeClass#getWert <em>Wert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wert</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.Taste_FGT_TypeClass#getWert()
	 * @see #getTaste_FGT_TypeClass()
	 * @generated
	 */
	EAttribute getTaste_FGT_TypeClass_Wert();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.Taste_WGT_TypeClass <em>Taste WGT Type Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Taste WGT Type Class</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.Taste_WGT_TypeClass
	 * @generated
	 */
	EClass getTaste_WGT_TypeClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.set.model.model1902.Nahbedienbereich.Taste_WGT_TypeClass#getWert <em>Wert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wert</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.Taste_WGT_TypeClass#getWert()
	 * @see #getTaste_WGT_TypeClass()
	 * @generated
	 */
	EAttribute getTaste_WGT_TypeClass_Wert();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.WHU_TypeClass <em>WHU Type Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>WHU Type Class</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.WHU_TypeClass
	 * @generated
	 */
	EClass getWHU_TypeClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.set.model.model1902.Nahbedienbereich.WHU_TypeClass#getWert <em>Wert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wert</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.WHU_TypeClass#getWert()
	 * @see #getWHU_TypeClass()
	 * @generated
	 */
	EAttribute getWHU_TypeClass_Wert();

	/**
	 * Returns the meta object for class '{@link org.eclipse.set.model.model1902.Nahbedienbereich.WUS_TypeClass <em>WUS Type Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>WUS Type Class</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.WUS_TypeClass
	 * @generated
	 */
	EClass getWUS_TypeClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.set.model.model1902.Nahbedienbereich.WUS_TypeClass#getWert <em>Wert</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wert</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.WUS_TypeClass#getWert()
	 * @see #getWUS_TypeClass()
	 * @generated
	 */
	EAttribute getWUS_TypeClass_Wert();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBArt <em>ENUMNB Art</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>ENUMNB Art</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBArt
	 * @generated
	 */
	EEnum getENUMNBArt();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBGrenzeArt <em>ENUMNB Grenze Art</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>ENUMNB Grenze Art</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBGrenzeArt
	 * @generated
	 */
	EEnum getENUMNBGrenzeArt();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBRueckgabevoraussetzung <em>ENUMNB Rueckgabevoraussetzung</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>ENUMNB Rueckgabevoraussetzung</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBRueckgabevoraussetzung
	 * @generated
	 */
	EEnum getENUMNBRueckgabevoraussetzung();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBVerhaeltnisBesonders <em>ENUMNB Verhaeltnis Besonders</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>ENUMNB Verhaeltnis Besonders</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBVerhaeltnisBesonders
	 * @generated
	 */
	EEnum getENUMNBVerhaeltnisBesonders();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBArt <em>ENUMNB Art Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>ENUMNB Art Object</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBArt
	 * @model instanceClass="org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBArt"
	 *        extendedMetaData="name='ENUMNB_Art:Object' baseType='ENUMNB_Art'"
	 * @generated
	 */
	EDataType getENUMNBArtObject();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBGrenzeArt <em>ENUMNB Grenze Art Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>ENUMNB Grenze Art Object</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBGrenzeArt
	 * @model instanceClass="org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBGrenzeArt"
	 *        extendedMetaData="name='ENUMNB_Grenze_Art:Object' baseType='ENUMNB_Grenze_Art'"
	 * @generated
	 */
	EDataType getENUMNBGrenzeArtObject();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBRueckgabevoraussetzung <em>ENUMNB Rueckgabevoraussetzung Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>ENUMNB Rueckgabevoraussetzung Object</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBRueckgabevoraussetzung
	 * @model instanceClass="org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBRueckgabevoraussetzung"
	 *        extendedMetaData="name='ENUMNB_Rueckgabevoraussetzung:Object' baseType='ENUMNB_Rueckgabevoraussetzung'"
	 * @generated
	 */
	EDataType getENUMNBRueckgabevoraussetzungObject();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBVerhaeltnisBesonders <em>ENUMNB Verhaeltnis Besonders Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>ENUMNB Verhaeltnis Besonders Object</em>'.
	 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBVerhaeltnisBesonders
	 * @model instanceClass="org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBVerhaeltnisBesonders"
	 *        extendedMetaData="name='ENUMNB_Verhaeltnis_Besonders:Object' baseType='ENUMNB_Verhaeltnis_Besonders'"
	 * @generated
	 */
	EDataType getENUMNBVerhaeltnisBesondersObject();

	/**
	 * Returns the meta object for data type '{@link java.math.BigInteger <em>NB Bezeichnung Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>NB Bezeichnung Type</em>'.
	 * @see java.math.BigInteger
	 * @model instanceClass="java.math.BigInteger"
	 *        extendedMetaData="name='TNB_Bezeichnung' baseType='http://www.eclipse.org/emf/2003/XMLType#integer' pattern='[1-9][0-9]?'"
	 * @generated
	 */
	EDataType getNB_Bezeichnung_Type();

	/**
	 * Returns the meta object for data type '{@link java.math.BigInteger <em>NB Zone Allg Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>NB Zone Allg Type</em>'.
	 * @see java.math.BigInteger
	 * @model instanceClass="java.math.BigInteger"
	 *        extendedMetaData="name='TNB_Zone_Allg' baseType='http://www.eclipse.org/emf/2003/XMLType#integer' pattern='[1-9][0-9]*'"
	 * @generated
	 */
	EDataType getNB_Zone_Allg_Type();

	/**
	 * Returns the meta object for data type '{@link java.math.BigInteger <em>NB Zone Bezeichnung Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>NB Zone Bezeichnung Type</em>'.
	 * @see java.math.BigInteger
	 * @model instanceClass="java.math.BigInteger"
	 *        extendedMetaData="name='TNB_Zone_Bezeichnung' baseType='http://www.eclipse.org/emf/2003/XMLType#integer' pattern='[1-9][0-9]?'"
	 * @generated
	 */
	EDataType getNB_Zone_Bezeichnung_Type();

	/**
	 * Returns the meta object for data type '{@link java.math.BigInteger <em>Rang Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Rang Type</em>'.
	 * @see java.math.BigInteger
	 * @model instanceClass="java.math.BigInteger"
	 *        extendedMetaData="name='TRang' baseType='http://www.eclipse.org/emf/2003/XMLType#integer' pattern='[1-9][0-9]{0,}'"
	 * @generated
	 */
	EDataType getRang_Type();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	NahbedienbereichFactory getNahbedienbereichFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.AWU_TypeClassImpl <em>AWU Type Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.AWU_TypeClassImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getAWU_TypeClass()
		 * @generated
		 */
		EClass AWU_TYPE_CLASS = eINSTANCE.getAWU_TypeClass();

		/**
		 * The meta object literal for the '<em><b>Wert</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AWU_TYPE_CLASS__WERT = eINSTANCE.getAWU_TypeClass_Wert();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.F_ST_Z_TypeClassImpl <em>FST ZType Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.F_ST_Z_TypeClassImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getF_ST_Z_TypeClass()
		 * @generated
		 */
		EClass FST_ZTYPE_CLASS = eINSTANCE.getF_ST_Z_TypeClass();

		/**
		 * The meta object literal for the '<em><b>Wert</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FST_ZTYPE_CLASS__WERT = eINSTANCE.getF_ST_Z_TypeClass_Wert();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.FA_FAE_TypeClassImpl <em>FA FAE Type Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.FA_FAE_TypeClassImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getFA_FAE_TypeClass()
		 * @generated
		 */
		EClass FA_FAE_TYPE_CLASS = eINSTANCE.getFA_FAE_TypeClass();

		/**
		 * The meta object literal for the '<em><b>Wert</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FA_FAE_TYPE_CLASS__WERT = eINSTANCE.getFA_FAE_TypeClass_Wert();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.Freie_Stellbarkeit_TypeClassImpl <em>Freie Stellbarkeit Type Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.Freie_Stellbarkeit_TypeClassImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getFreie_Stellbarkeit_TypeClass()
		 * @generated
		 */
		EClass FREIE_STELLBARKEIT_TYPE_CLASS = eINSTANCE.getFreie_Stellbarkeit_TypeClass();

		/**
		 * The meta object literal for the '<em><b>Wert</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREIE_STELLBARKEIT_TYPE_CLASS__WERT = eINSTANCE.getFreie_Stellbarkeit_TypeClass_Wert();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NBImpl <em>NB</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NBImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB()
		 * @generated
		 */
		EClass NB = eINSTANCE.getNB();

		/**
		 * The meta object literal for the '<em><b>Bezeichnung</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB__BEZEICHNUNG = eINSTANCE.getNB_Bezeichnung();

		/**
		 * The meta object literal for the '<em><b>NB Allg</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB__NB_ALLG = eINSTANCE.getNB_NBAllg();

		/**
		 * The meta object literal for the '<em><b>NB Funktionalitaet NBR</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB__NB_FUNKTIONALITAET_NBR = eINSTANCE.getNB_NBFunktionalitaetNBR();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Allg_AttributeGroupImpl <em>NB Allg Attribute Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Allg_AttributeGroupImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Allg_AttributeGroup()
		 * @generated
		 */
		EClass NB_ALLG_ATTRIBUTE_GROUP = eINSTANCE.getNB_Allg_AttributeGroup();

		/**
		 * The meta object literal for the '<em><b>NB Art</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_ALLG_ATTRIBUTE_GROUP__NB_ART = eINSTANCE.getNB_Allg_AttributeGroup_NBArt();

		/**
		 * The meta object literal for the '<em><b>NB Bezeichnung</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_ALLG_ATTRIBUTE_GROUP__NB_BEZEICHNUNG = eINSTANCE.getNB_Allg_AttributeGroup_NBBezeichnung();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Art_TypeClassImpl <em>NB Art Type Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Art_TypeClassImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Art_TypeClass()
		 * @generated
		 */
		EClass NB_ART_TYPE_CLASS = eINSTANCE.getNB_Art_TypeClass();

		/**
		 * The meta object literal for the '<em><b>Wert</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NB_ART_TYPE_CLASS__WERT = eINSTANCE.getNB_Art_TypeClass_Wert();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Bedien_Anzeige_ElementImpl <em>NB Bedien Anzeige Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Bedien_Anzeige_ElementImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Bedien_Anzeige_Element()
		 * @generated
		 */
		EClass NB_BEDIEN_ANZEIGE_ELEMENT = eINSTANCE.getNB_Bedien_Anzeige_Element();

		/**
		 * The meta object literal for the '<em><b>ID Bedien Anzeige Element</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_BEDIEN_ANZEIGE_ELEMENT__ID_BEDIEN_ANZEIGE_ELEMENT = eINSTANCE.getNB_Bedien_Anzeige_Element_IDBedienAnzeigeElement();

		/**
		 * The meta object literal for the '<em><b>IDNB Zone</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_BEDIEN_ANZEIGE_ELEMENT__IDNB_ZONE = eINSTANCE.getNB_Bedien_Anzeige_Element_IDNBZone();

		/**
		 * The meta object literal for the '<em><b>NB Bedien Anzeige Funktionen</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_BEDIEN_ANZEIGE_ELEMENT__NB_BEDIEN_ANZEIGE_FUNKTIONEN = eINSTANCE.getNB_Bedien_Anzeige_Element_NBBedienAnzeigeFunktionen();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Bedien_Anzeige_Funktionen_AttributeGroupImpl <em>NB Bedien Anzeige Funktionen Attribute Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Bedien_Anzeige_Funktionen_AttributeGroupImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Bedien_Anzeige_Funktionen_AttributeGroup()
		 * @generated
		 */
		EClass NB_BEDIEN_ANZEIGE_FUNKTIONEN_ATTRIBUTE_GROUP = eINSTANCE.getNB_Bedien_Anzeige_Funktionen_AttributeGroup();

		/**
		 * The meta object literal for the '<em><b>Taste ANF</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_BEDIEN_ANZEIGE_FUNKTIONEN_ATTRIBUTE_GROUP__TASTE_ANF = eINSTANCE.getNB_Bedien_Anzeige_Funktionen_AttributeGroup_TasteANF();

		/**
		 * The meta object literal for the '<em><b>Taste FGT</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_BEDIEN_ANZEIGE_FUNKTIONEN_ATTRIBUTE_GROUP__TASTE_FGT = eINSTANCE.getNB_Bedien_Anzeige_Funktionen_AttributeGroup_TasteFGT();

		/**
		 * The meta object literal for the '<em><b>Taste WGT</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_BEDIEN_ANZEIGE_FUNKTIONEN_ATTRIBUTE_GROUP__TASTE_WGT = eINSTANCE.getNB_Bedien_Anzeige_Funktionen_AttributeGroup_TasteWGT();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Bezeichnung_TypeClassImpl <em>NB Bezeichnung Type Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Bezeichnung_TypeClassImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Bezeichnung_TypeClass()
		 * @generated
		 */
		EClass NB_BEZEICHNUNG_TYPE_CLASS = eINSTANCE.getNB_Bezeichnung_TypeClass();

		/**
		 * The meta object literal for the '<em><b>Wert</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NB_BEZEICHNUNG_TYPE_CLASS__WERT = eINSTANCE.getNB_Bezeichnung_TypeClass_Wert();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Funktionalitaet_NB_R_AttributeGroupImpl <em>NB Funktionalitaet NB RAttribute Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Funktionalitaet_NB_R_AttributeGroupImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Funktionalitaet_NB_R_AttributeGroup()
		 * @generated
		 */
		EClass NB_FUNKTIONALITAET_NB_RATTRIBUTE_GROUP = eINSTANCE.getNB_Funktionalitaet_NB_R_AttributeGroup();

		/**
		 * The meta object literal for the '<em><b>AWU</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_FUNKTIONALITAET_NB_RATTRIBUTE_GROUP__AWU = eINSTANCE.getNB_Funktionalitaet_NB_R_AttributeGroup_AWU();

		/**
		 * The meta object literal for the '<em><b>FSTZ</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_FUNKTIONALITAET_NB_RATTRIBUTE_GROUP__FSTZ = eINSTANCE.getNB_Funktionalitaet_NB_R_AttributeGroup_FSTZ();

		/**
		 * The meta object literal for the '<em><b>FAFAE</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_FUNKTIONALITAET_NB_RATTRIBUTE_GROUP__FAFAE = eINSTANCE.getNB_Funktionalitaet_NB_R_AttributeGroup_FAFAE();

		/**
		 * The meta object literal for the '<em><b>SBUE</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_FUNKTIONALITAET_NB_RATTRIBUTE_GROUP__SBUE = eINSTANCE.getNB_Funktionalitaet_NB_R_AttributeGroup_SBUE();

		/**
		 * The meta object literal for the '<em><b>SLESLS</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_FUNKTIONALITAET_NB_RATTRIBUTE_GROUP__SLESLS = eINSTANCE.getNB_Funktionalitaet_NB_R_AttributeGroup_SLESLS();

		/**
		 * The meta object literal for the '<em><b>WHU</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_FUNKTIONALITAET_NB_RATTRIBUTE_GROUP__WHU = eINSTANCE.getNB_Funktionalitaet_NB_R_AttributeGroup_WHU();

		/**
		 * The meta object literal for the '<em><b>WUS</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_FUNKTIONALITAET_NB_RATTRIBUTE_GROUP__WUS = eINSTANCE.getNB_Funktionalitaet_NB_R_AttributeGroup_WUS();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Grenze_Art_TypeClassImpl <em>NB Grenze Art Type Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Grenze_Art_TypeClassImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Grenze_Art_TypeClass()
		 * @generated
		 */
		EClass NB_GRENZE_ART_TYPE_CLASS = eINSTANCE.getNB_Grenze_Art_TypeClass();

		/**
		 * The meta object literal for the '<em><b>Wert</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NB_GRENZE_ART_TYPE_CLASS__WERT = eINSTANCE.getNB_Grenze_Art_TypeClass_Wert();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Rueckgabevoraussetzung_TypeClassImpl <em>NB Rueckgabevoraussetzung Type Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Rueckgabevoraussetzung_TypeClassImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Rueckgabevoraussetzung_TypeClass()
		 * @generated
		 */
		EClass NB_RUECKGABEVORAUSSETZUNG_TYPE_CLASS = eINSTANCE.getNB_Rueckgabevoraussetzung_TypeClass();

		/**
		 * The meta object literal for the '<em><b>Wert</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NB_RUECKGABEVORAUSSETZUNG_TYPE_CLASS__WERT = eINSTANCE.getNB_Rueckgabevoraussetzung_TypeClass_Wert();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Verhaeltnis_Besonders_TypeClassImpl <em>NB Verhaeltnis Besonders Type Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Verhaeltnis_Besonders_TypeClassImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Verhaeltnis_Besonders_TypeClass()
		 * @generated
		 */
		EClass NB_VERHAELTNIS_BESONDERS_TYPE_CLASS = eINSTANCE.getNB_Verhaeltnis_Besonders_TypeClass();

		/**
		 * The meta object literal for the '<em><b>Wert</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NB_VERHAELTNIS_BESONDERS_TYPE_CLASS__WERT = eINSTANCE.getNB_Verhaeltnis_Besonders_TypeClass_Wert();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_ZoneImpl <em>NB Zone</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_ZoneImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone()
		 * @generated
		 */
		EClass NB_ZONE = eINSTANCE.getNB_Zone();

		/**
		 * The meta object literal for the '<em><b>IDNB</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_ZONE__IDNB = eINSTANCE.getNB_Zone_IDNB();

		/**
		 * The meta object literal for the '<em><b>IDNB Zone</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_ZONE__IDNB_ZONE = eINSTANCE.getNB_Zone_IDNBZone();

		/**
		 * The meta object literal for the '<em><b>NB Zone Allg</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_ZONE__NB_ZONE_ALLG = eINSTANCE.getNB_Zone_NBZoneAllg();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Allg_AttributeGroupImpl <em>NB Zone Allg Attribute Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Allg_AttributeGroupImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone_Allg_AttributeGroup()
		 * @generated
		 */
		EClass NB_ZONE_ALLG_ATTRIBUTE_GROUP = eINSTANCE.getNB_Zone_Allg_AttributeGroup();

		/**
		 * The meta object literal for the '<em><b>NB Verhaeltnis Besonders</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_ZONE_ALLG_ATTRIBUTE_GROUP__NB_VERHAELTNIS_BESONDERS = eINSTANCE.getNB_Zone_Allg_AttributeGroup_NBVerhaeltnisBesonders();

		/**
		 * The meta object literal for the '<em><b>NB Zone Bezeichnung</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_ZONE_ALLG_ATTRIBUTE_GROUP__NB_ZONE_BEZEICHNUNG = eINSTANCE.getNB_Zone_Allg_AttributeGroup_NBZoneBezeichnung();

		/**
		 * The meta object literal for the '<em><b>Rang</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_ZONE_ALLG_ATTRIBUTE_GROUP__RANG = eINSTANCE.getNB_Zone_Allg_AttributeGroup_Rang();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Allg_TypeClassImpl <em>NB Zone Allg Type Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Allg_TypeClassImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone_Allg_TypeClass()
		 * @generated
		 */
		EClass NB_ZONE_ALLG_TYPE_CLASS = eINSTANCE.getNB_Zone_Allg_TypeClass();

		/**
		 * The meta object literal for the '<em><b>Wert</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NB_ZONE_ALLG_TYPE_CLASS__WERT = eINSTANCE.getNB_Zone_Allg_TypeClass_Wert();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Bezeichnung_TypeClassImpl <em>NB Zone Bezeichnung Type Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Bezeichnung_TypeClassImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone_Bezeichnung_TypeClass()
		 * @generated
		 */
		EClass NB_ZONE_BEZEICHNUNG_TYPE_CLASS = eINSTANCE.getNB_Zone_Bezeichnung_TypeClass();

		/**
		 * The meta object literal for the '<em><b>Wert</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NB_ZONE_BEZEICHNUNG_TYPE_CLASS__WERT = eINSTANCE.getNB_Zone_Bezeichnung_TypeClass_Wert();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_ElementImpl <em>NB Zone Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_ElementImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone_Element()
		 * @generated
		 */
		EClass NB_ZONE_ELEMENT = eINSTANCE.getNB_Zone_Element();

		/**
		 * The meta object literal for the '<em><b>IDNB Element</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_ZONE_ELEMENT__IDNB_ELEMENT = eINSTANCE.getNB_Zone_Element_IDNBElement();

		/**
		 * The meta object literal for the '<em><b>IDNB Zone</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_ZONE_ELEMENT__IDNB_ZONE = eINSTANCE.getNB_Zone_Element_IDNBZone();

		/**
		 * The meta object literal for the '<em><b>NB Zone Element Allg</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_ZONE_ELEMENT__NB_ZONE_ELEMENT_ALLG = eINSTANCE.getNB_Zone_Element_NBZoneElementAllg();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Element_Allg_AttributeGroupImpl <em>NB Zone Element Allg Attribute Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Element_Allg_AttributeGroupImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone_Element_Allg_AttributeGroup()
		 * @generated
		 */
		EClass NB_ZONE_ELEMENT_ALLG_ATTRIBUTE_GROUP = eINSTANCE.getNB_Zone_Element_Allg_AttributeGroup();

		/**
		 * The meta object literal for the '<em><b>Freie Stellbarkeit</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_ZONE_ELEMENT_ALLG_ATTRIBUTE_GROUP__FREIE_STELLBARKEIT = eINSTANCE.getNB_Zone_Element_Allg_AttributeGroup_FreieStellbarkeit();

		/**
		 * The meta object literal for the '<em><b>NB Rueckgabevoraussetzung</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_ZONE_ELEMENT_ALLG_ATTRIBUTE_GROUP__NB_RUECKGABEVORAUSSETZUNG = eINSTANCE.getNB_Zone_Element_Allg_AttributeGroup_NBRueckgabevoraussetzung();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_GrenzeImpl <em>NB Zone Grenze</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_GrenzeImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone_Grenze()
		 * @generated
		 */
		EClass NB_ZONE_GRENZE = eINSTANCE.getNB_Zone_Grenze();

		/**
		 * The meta object literal for the '<em><b>ID Markanter Punkt</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_ZONE_GRENZE__ID_MARKANTER_PUNKT = eINSTANCE.getNB_Zone_Grenze_IDMarkanterPunkt();

		/**
		 * The meta object literal for the '<em><b>IDNB Zone</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_ZONE_GRENZE__IDNB_ZONE = eINSTANCE.getNB_Zone_Grenze_IDNBZone();

		/**
		 * The meta object literal for the '<em><b>NB Grenze Art</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_ZONE_GRENZE__NB_GRENZE_ART = eINSTANCE.getNB_Zone_Grenze_NBGrenzeArt();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Reihenfolgezwang_AttributeGroupImpl <em>NB Zone Reihenfolgezwang Attribute Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NB_Zone_Reihenfolgezwang_AttributeGroupImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone_Reihenfolgezwang_AttributeGroup()
		 * @generated
		 */
		EClass NB_ZONE_REIHENFOLGEZWANG_ATTRIBUTE_GROUP = eINSTANCE.getNB_Zone_Reihenfolgezwang_AttributeGroup();

		/**
		 * The meta object literal for the '<em><b>NB Zone Allg</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NB_ZONE_REIHENFOLGEZWANG_ATTRIBUTE_GROUP__NB_ZONE_ALLG = eINSTANCE.getNB_Zone_Reihenfolgezwang_AttributeGroup_NBZoneAllg();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.Rang_TypeClassImpl <em>Rang Type Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.Rang_TypeClassImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getRang_TypeClass()
		 * @generated
		 */
		EClass RANG_TYPE_CLASS = eINSTANCE.getRang_TypeClass();

		/**
		 * The meta object literal for the '<em><b>Wert</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RANG_TYPE_CLASS__WERT = eINSTANCE.getRang_TypeClass_Wert();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.SBUE_TypeClassImpl <em>SBUE Type Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.SBUE_TypeClassImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getSBUE_TypeClass()
		 * @generated
		 */
		EClass SBUE_TYPE_CLASS = eINSTANCE.getSBUE_TypeClass();

		/**
		 * The meta object literal for the '<em><b>Wert</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SBUE_TYPE_CLASS__WERT = eINSTANCE.getSBUE_TypeClass_Wert();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.SLE_SLS_TypeClassImpl <em>SLE SLS Type Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.SLE_SLS_TypeClassImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getSLE_SLS_TypeClass()
		 * @generated
		 */
		EClass SLE_SLS_TYPE_CLASS = eINSTANCE.getSLE_SLS_TypeClass();

		/**
		 * The meta object literal for the '<em><b>Wert</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SLE_SLS_TYPE_CLASS__WERT = eINSTANCE.getSLE_SLS_TypeClass_Wert();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.Taste_ANF_TypeClassImpl <em>Taste ANF Type Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.Taste_ANF_TypeClassImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getTaste_ANF_TypeClass()
		 * @generated
		 */
		EClass TASTE_ANF_TYPE_CLASS = eINSTANCE.getTaste_ANF_TypeClass();

		/**
		 * The meta object literal for the '<em><b>Wert</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASTE_ANF_TYPE_CLASS__WERT = eINSTANCE.getTaste_ANF_TypeClass_Wert();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.Taste_FGT_TypeClassImpl <em>Taste FGT Type Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.Taste_FGT_TypeClassImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getTaste_FGT_TypeClass()
		 * @generated
		 */
		EClass TASTE_FGT_TYPE_CLASS = eINSTANCE.getTaste_FGT_TypeClass();

		/**
		 * The meta object literal for the '<em><b>Wert</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASTE_FGT_TYPE_CLASS__WERT = eINSTANCE.getTaste_FGT_TypeClass_Wert();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.Taste_WGT_TypeClassImpl <em>Taste WGT Type Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.Taste_WGT_TypeClassImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getTaste_WGT_TypeClass()
		 * @generated
		 */
		EClass TASTE_WGT_TYPE_CLASS = eINSTANCE.getTaste_WGT_TypeClass();

		/**
		 * The meta object literal for the '<em><b>Wert</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASTE_WGT_TYPE_CLASS__WERT = eINSTANCE.getTaste_WGT_TypeClass_Wert();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.WHU_TypeClassImpl <em>WHU Type Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.WHU_TypeClassImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getWHU_TypeClass()
		 * @generated
		 */
		EClass WHU_TYPE_CLASS = eINSTANCE.getWHU_TypeClass();

		/**
		 * The meta object literal for the '<em><b>Wert</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WHU_TYPE_CLASS__WERT = eINSTANCE.getWHU_TypeClass_Wert();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.impl.WUS_TypeClassImpl <em>WUS Type Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.WUS_TypeClassImpl
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getWUS_TypeClass()
		 * @generated
		 */
		EClass WUS_TYPE_CLASS = eINSTANCE.getWUS_TypeClass();

		/**
		 * The meta object literal for the '<em><b>Wert</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WUS_TYPE_CLASS__WERT = eINSTANCE.getWUS_TypeClass_Wert();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBArt <em>ENUMNB Art</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBArt
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getENUMNBArt()
		 * @generated
		 */
		EEnum ENUMNB_ART = eINSTANCE.getENUMNBArt();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBGrenzeArt <em>ENUMNB Grenze Art</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBGrenzeArt
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getENUMNBGrenzeArt()
		 * @generated
		 */
		EEnum ENUMNB_GRENZE_ART = eINSTANCE.getENUMNBGrenzeArt();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBRueckgabevoraussetzung <em>ENUMNB Rueckgabevoraussetzung</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBRueckgabevoraussetzung
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getENUMNBRueckgabevoraussetzung()
		 * @generated
		 */
		EEnum ENUMNB_RUECKGABEVORAUSSETZUNG = eINSTANCE.getENUMNBRueckgabevoraussetzung();

		/**
		 * The meta object literal for the '{@link org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBVerhaeltnisBesonders <em>ENUMNB Verhaeltnis Besonders</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBVerhaeltnisBesonders
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getENUMNBVerhaeltnisBesonders()
		 * @generated
		 */
		EEnum ENUMNB_VERHAELTNIS_BESONDERS = eINSTANCE.getENUMNBVerhaeltnisBesonders();

		/**
		 * The meta object literal for the '<em>ENUMNB Art Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBArt
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getENUMNBArtObject()
		 * @generated
		 */
		EDataType ENUMNB_ART_OBJECT = eINSTANCE.getENUMNBArtObject();

		/**
		 * The meta object literal for the '<em>ENUMNB Grenze Art Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBGrenzeArt
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getENUMNBGrenzeArtObject()
		 * @generated
		 */
		EDataType ENUMNB_GRENZE_ART_OBJECT = eINSTANCE.getENUMNBGrenzeArtObject();

		/**
		 * The meta object literal for the '<em>ENUMNB Rueckgabevoraussetzung Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBRueckgabevoraussetzung
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getENUMNBRueckgabevoraussetzungObject()
		 * @generated
		 */
		EDataType ENUMNB_RUECKGABEVORAUSSETZUNG_OBJECT = eINSTANCE.getENUMNBRueckgabevoraussetzungObject();

		/**
		 * The meta object literal for the '<em>ENUMNB Verhaeltnis Besonders Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.ENUMNBVerhaeltnisBesonders
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getENUMNBVerhaeltnisBesondersObject()
		 * @generated
		 */
		EDataType ENUMNB_VERHAELTNIS_BESONDERS_OBJECT = eINSTANCE.getENUMNBVerhaeltnisBesondersObject();

		/**
		 * The meta object literal for the '<em>NB Bezeichnung Type</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.math.BigInteger
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Bezeichnung_Type()
		 * @generated
		 */
		EDataType NB_BEZEICHNUNG_TYPE = eINSTANCE.getNB_Bezeichnung_Type();

		/**
		 * The meta object literal for the '<em>NB Zone Allg Type</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.math.BigInteger
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone_Allg_Type()
		 * @generated
		 */
		EDataType NB_ZONE_ALLG_TYPE = eINSTANCE.getNB_Zone_Allg_Type();

		/**
		 * The meta object literal for the '<em>NB Zone Bezeichnung Type</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.math.BigInteger
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getNB_Zone_Bezeichnung_Type()
		 * @generated
		 */
		EDataType NB_ZONE_BEZEICHNUNG_TYPE = eINSTANCE.getNB_Zone_Bezeichnung_Type();

		/**
		 * The meta object literal for the '<em>Rang Type</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.math.BigInteger
		 * @see org.eclipse.set.model.model1902.Nahbedienbereich.impl.NahbedienbereichPackageImpl#getRang_Type()
		 * @generated
		 */
		EDataType RANG_TYPE = eINSTANCE.getRang_Type();

	}

} //NahbedienbereichPackage
